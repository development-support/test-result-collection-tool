VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Sheet4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Private Sub Worksheet_Activate()

End Sub

Private Sub Worksheet_BeforeDoubleClick(ByVal Target As Range, Cancel As Boolean)

End Sub

Private Sub Worksheet_Change(ByVal Target As Range)
    If Target.row = 29 And Target.Column = 1 Then
       If Target.Value = "Weekly" Or Target.Value = "Bi-Weekly" Then
            Application.ScreenUpdating = False
            'Worksheets("Test Data").Unprotect Password:="qpm"
            Rows("1:1").EntireRow.Hidden = True
            Rows("2:2").EntireRow.Hidden = False
            'Worksheets("Test Data").Protect Password:="qpm"
            Application.ScreenUpdating = True
        Else
            Application.ScreenUpdating = False
            'Worksheets("Test Data").Unprotect Password:="qpm"
            Rows("2:2").EntireRow.Hidden = True
            Rows("1:1").EntireRow.Hidden = False
            'Worksheets("Test Data").Protect Password:="qpm"
            Application.ScreenUpdating = True
        End If
    End If
End Sub

