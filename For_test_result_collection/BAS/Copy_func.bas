Attribute VB_Name = "Copy_func"
Option Explicit
' 2016/09/06 Create New :Dataコピー用マクロ

Sub Planed_copy()
  Worksheets("Planed_Data").Range("B2:BZ13").Copy
  Worksheets("Test Data").Range("B3").PasteSpecial Paste:=xlValues

  Worksheets("Planed_Data").Range("B24:BZ24").Copy
  Worksheets("Test Data").Range("B25").PasteSpecial Paste:=xlValues
End Sub
