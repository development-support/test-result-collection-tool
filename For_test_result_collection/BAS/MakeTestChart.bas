Attribute VB_Name = "MakeTestChart"
Option Explicit
' 10/31/08 asandler Updated to address textbox refresh problem


Public Sub updateChart()
Attribute updateChart.VB_ProcData.VB_Invoke_Func = " \n14"
' 07/29/09 asandler updated for improved chart with attempts
' 01/06/10 - updates with data check and charts for NE
  Dim vMsg As String
  Dim myNull
  myNull = Null
  Dim vCurrentCol As Integer
  Dim vCurrentDate As Date
  Dim vCounter As Integer
  Dim vCheckCol As Integer
  Dim vCheckDate As Integer
  Dim LastCol As Integer
  Dim vCheckPassRate As Integer
  Dim inputSh As String
  Dim outputSh As String
  Dim i As Long
  Dim vTitle As String
  Dim vResponce As String
  Dim vcheck As Long


  Application.ScreenUpdating = False
  inputSh = Worksheets("Terminology").Cells(1, 26).Value
  outputSh = Worksheets("Terminology").Cells(1, 24).Value

  i = 1
  While (Sheets(inputSh).Cells(2, i).Value <> "")
    i = i + 1
  Wend

  LastCol = i - 1

  ' check if current date on pick list
  vCurrentDate = Sheets(inputSh).Range("A34").Value
  vCheckDate = 0

  For i = 2 To LastCol
    If Sheets(inputSh).Cells(2, i).Value = vCurrentDate Then
      vCheckDate = 1
      Exit For
    End If
  Next

  If vCheckDate = 0 Then
    vTitle = "Alert"
    vMsg = "Current date value is not on pick list line 34"
    Sheets(inputSh).Select
    vResponce = MsgBox(vMsg, vbCritical, vTitle)

    GoTo LastLine
  End If

  ' calculate Columns for current date
  vCounter = 2

  vCurrentCol = 0
  While (Sheets(inputSh).Cells(2, vCounter).Value <> "")
    If Sheets(inputSh).Cells(2, vCounter).Value <> vCurrentDate Then
      vCounter = vCounter + 1
    Else
      vCurrentCol = vCounter
      vCounter = LastCol + 1
    End If
  Wend

  If vCurrentCol > 2 Then
    'checking data completness
    '----------------------------
    vcheck = fDataCheck(3, LastCol, "Total Test Planned")
    If vcheck = 1 Then GoTo LastLine

    vcheck = fDataCheck(5, LastCol, "Estimated Weekly Pass Rate")
    If vcheck = 1 Then GoTo LastLine

    vcheck = fDataCheck(6, LastCol, "Estimated Weekly Attempts")
    If vcheck = 1 Then GoTo LastLine

    vcheck = fDataCheck(7, LastCol, "Planned Tests")
    If vcheck = 1 Then GoTo LastLine

    vcheck = fDataCheck(8, LastCol, "Planned Passed Test")
    If vcheck = 1 Then GoTo LastLine

    vcheck = fDataCheck(9, vCurrentCol, "Actual Attempts")
    If vcheck = 1 Then GoTo LastLine

    vcheck = fDataCheck(10, vCurrentCol, "Passed")
    If vcheck = 1 Then GoTo LastLine

    vcheck = fDataCheck(11, vCurrentCol, "Blocked")
    If vcheck = 1 Then GoTo LastLine
 
    vcheck = fDataCheck(12, vCurrentCol, "Failed")
    If vcheck = 1 Then GoTo LastLine

    vcheck = fDataCheck(13, vCurrentCol, "Failed (Ready for ReTest)")
    If vcheck = 1 Then GoTo LastLine

    vcheck = fDataCheck(14, vCurrentCol, "Ready (Not Attempted)")
    If vcheck = 1 Then GoTo LastLine

    vcheck = fDataCheck(20, vCurrentCol, "CR/MJ PTRs UnResolved")
    If vcheck = 1 Then GoTo LastLine

    vcheck = fDataCheck(21, vCurrentCol, "CR/MJ PTRs Resolved")
    If vcheck = 1 Then GoTo LastLine

    vcheck = fDataCheck(22, vCurrentCol, "CR/MJ PTRs  Submitted (weekly")
    If vcheck = 1 Then GoTo LastLine
    '------- end of data completness check

    ' check for weekly pass rate above 100%

    ' check for weekly pass rate above 100%
    If i > 2 Then
      vCheckPassRate = fPassRateCheck(19, vCurrentCol, "Weekly Pass Rate")
      If vCheckPassRate = 1 Then GoTo LastLine
    End If

  End If

  Sheets(outputSh).Activate
  ActiveSheet.ChartObjects("Chart 1").Activate
  ActiveChart.ChartArea.Select

  With Worksheets(inputSh)
    ActiveChart.SeriesCollection(1).XValues = .Range(.Cells(2, 2), .Cells(1, LastCol)) ' Date
    ActiveChart.SeriesCollection(2).XValues = .Range(.Cells(2, 2), .Cells(1, LastCol)) ' Date
    ActiveChart.SeriesCollection(3).XValues = .Range(.Cells(2, 2), .Cells(1, LastCol)) ' Date
    ActiveChart.SeriesCollection(4).XValues = .Range(.Cells(2, 2), .Cells(1, LastCol)) ' Date
    ActiveChart.SeriesCollection(5).XValues = .Range(.Cells(2, 2), .Cells(1, LastCol)) ' Date
    ActiveChart.SeriesCollection(6).XValues = .Range(.Cells(2, 2), .Cells(1, LastCol)) ' Date
    ActiveChart.SeriesCollection(7).XValues = .Range(.Cells(2, 2), .Cells(1, LastCol)) ' Date
    ActiveChart.SeriesCollection(8).XValues = .Range(.Cells(2, 2), .Cells(1, LastCol)) ' Date
    ActiveChart.SeriesCollection(9).XValues = .Range(.Cells(2, 2), .Cells(1, LastCol)) ' Date
    ActiveChart.SeriesCollection(10).XValues = .Range(.Cells(2, 2), .Cells(1, LastCol)) ' Date
    ActiveChart.SeriesCollection(11).XValues = .Range(.Cells(2, 2), .Cells(1, LastCol)) ' Date
    ActiveChart.SeriesCollection(12).XValues = .Range(.Cells(2, 2), .Cells(1, LastCol)) ' Date
    ActiveChart.SeriesCollection(13).XValues = .Range(.Cells(2, 2), .Cells(1, LastCol)) ' Date
    ActiveChart.SeriesCollection(14).XValues = .Range(.Cells(2, 2), .Cells(1, LastCol)) ' Date

    ActiveChart.SeriesCollection(7).Values = .Range(.Cells(3, 2), .Cells(3, LastCol)) ' Total Tests Planned
    ActiveChart.SeriesCollection(6).Values = .Range(.Cells(7, 2), .Cells(7, LastCol)) ' Planned Tests
    ActiveChart.SeriesCollection(5).Values = .Range(.Cells(8, 2), .Cells(8, LastCol)) ' Planned Pass
    ActiveChart.SeriesCollection(9).Values = Range(.Cells(9, 2), .Cells(9, vCurrentCol)) ' Actual Attempts
    ActiveChart.SeriesCollection(1).Values = Range(.Cells(10, 2), .Cells(10, vCurrentCol)) ' Pass
    ActiveChart.SeriesCollection(4).Values = .Range(.Cells(11, 2), .Cells(11, vCurrentCol)) ' Blocked
    ActiveChart.SeriesCollection(3).Values = .Range(.Cells(12, 2), .Cells(12, vCurrentCol)) ' Fail
    ActiveChart.SeriesCollection(2).Values = Range(.Cells(13, 2), .Cells(13, vCurrentCol)) ' Failed readyForRetest
    ActiveChart.SeriesCollection(8).Values = Range(.Cells(14, 2), .Cells(14, vCurrentCol)) ' Ready
    ActiveChart.SeriesCollection(10).Values = Range(.Cells(18, 2), .Cells(18, vCurrentCol)) ' Cumulative Pass Rate
    ActiveChart.SeriesCollection(11).Values = Range(.Cells(19, 2), .Cells(19, vCurrentCol)) ' Weekly Pass Rate
    ActiveChart.SeriesCollection(12).Values = Range(.Cells(20, 2), .Cells(20, vCurrentCol)) ' CR/MJ unresolved PTRs
    ActiveChart.SeriesCollection(13).Values = Range(.Cells(21, 2), .Cells(21, vCurrentCol)) ' CR/MJ resolved PTRs
    ActiveChart.SeriesCollection(14).Values = Range(.Cells(22, 2), .Cells(22, vCurrentCol)) ' CR/MJ submited

    ActiveSheet.Range("A1").Select
  End With

  ' added 10/31/02 to refresh text box in Excel 2007
  '-------------------------------------------
  Worksheets("Test Chart").Shapes("Text Box 6").Select
  Selection.Formula = "'Chart Info'!B15"
  Selection.Font.Size = 14
  Worksheets("Test Chart").Shapes("Text Box 2").Select
  Selection.Formula = "'Chart Info'!B2"
  Selection.Font.Bold = True
  Selection.Font.Size = 20
  '---------------------------------------------------

  Application.ScreenUpdating = True
  Worksheets("Test Chart").Range("A1").Select
LastLine:

End Sub


Public Sub toggleHelp()
Attribute toggleHelp.VB_ProcData.VB_Invoke_Func = " \n14"
  If Worksheets("Test Data").Shapes("InstButton").TextFrame.Characters.Text = "Hide Instructions" Then
    Worksheets("Test Data").Shapes("InstText").Visible = False
    Worksheets("Test Data").Shapes("InstButton").TextFrame.Characters.Text = "Show Instructions"
  Else ' Show instructions
    Worksheets("Test Data").Shapes("InstText").Visible = True
    Worksheets("Test Data").Shapes("InstButton").TextFrame.Characters.Text = "Hide Instructions"
  End If
End Sub


Private Function fDataCheck(ByVal vLine As Long, _
                            ByVal vCol As Long, _
                            ByVal vText As String) As Long
  Dim ReturnValue
  Dim vMsg As String
  Dim myNull
  myNull = Null
  Dim inputSh As String
  Dim vCheckCol As Long
  Dim vTitle As String
  Dim vResponce As String

  inputSh = Worksheets("Terminology").Cells(1, 26).Value

  vCheckCol = 2

  While vCheckCol <= vCol
    If Sheets(inputSh).Cells(vLine, vCheckCol).Value = "" _
    Then GoTo Line3 Else GoTo Line4

Line3:
    vTitle = "Alert"
    vMsg = "Enter " & vText & " in every cell within highlited area in line " & vLine & "."
    Sheets(inputSh).Select
    vResponce = MsgBox(vMsg, vbCritical, vTitle)
    fDataCheck = 1
    Exit Function
Line4:
    vCheckCol = vCheckCol + 1
  Wend

  fDataCheck = 0

End Function


Private Function fPassRateCheck(ByVal vLine As Long, _
                                ByVal vCol As Long, _
                                ByVal vText As String) As Long
  Dim ReturnValue
  Dim vMsg As String
  Dim myNull
  myNull = Null
  Dim inputSh As String
  Dim vTitle As String
  Dim vResponce As String

  inputSh = Worksheets("Terminology").Cells(1, 26).Value

  If Sheets(inputSh).Cells(vLine, vCol).Value < 0 Then
    vTitle = "Alert"
    vMsg = vText & " for week of " & Sheets(inputSh).Cells(2, vCol).Value & " is below 0. Please check your data."
    Sheets(inputSh).Select
    vResponce = MsgBox(vMsg, vbCritical, vTitle)
    fPassRateCheck = 1
    Exit Function
  ElseIf Sheets(inputSh).Cells(vLine, vCol).Value > 1 Then
    vTitle = "Alert"
    vMsg = vText & " for week of " & Sheets(inputSh).Cells(2, vCol).Value & " is above 100%. Please check your data."
    Sheets(inputSh).Select
    vResponce = MsgBox(vMsg, vbCritical, vTitle)
    fPassRateCheck = 1
    Exit Function
  End If

  fPassRateCheck = 0

End Function

