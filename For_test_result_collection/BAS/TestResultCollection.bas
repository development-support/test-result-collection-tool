Attribute VB_Name = "TestResultCollection"
Option Explicit

' 下記の2行の内のどちらかをコメントにして下さい！
#Const cnsTest = 0  ' ←本番
'#Const cnsTest = 1   ' ←テスト

' 各項目書の集計シート名
Public Const gEachItemSheet As String = "集計"
' 集計用シート名
Public Const gAggregationSheet As String = "Data_Aggregation"
' テンプレート用シート名
Public Const gTemplateSheet As String = "TemplateOfData"
' 各項目書データ用のシート名
Public Const gItemSheet As String = "ItemData"
' 一時集計マージ用シート名
Public Const gTempSheet As String = "TempData"
' 集計マージ用シート名
Public Const gMergeSheet As String = "MergeData"
' Test Dataシート名
Public Const gTestData As String = "Test Data"

' 集計シートの項目行
Public Enum eRowItems
  Date = 1
  TotalTestsPlanned  ' 全期間で同値
  PlannedTests       ' 累計対象
  PlannedPass        ' 累計対象
  ActualAttempts     ' 累計対象
  Passed             ' 累計対象
  Blocked
  Failed
  FailedReady4ReTest
  ReadyNotAttempted
  Executed           ' 累計対象
  PTRsUnResolved     ' 累計対象
  PTRsResolved       ' 累計対象
  PTRsSubmittedDaily
  DailyBug
End Enum

' 指定ディレクトリ情報の定義型
Type tDirInfo
  FullPath As String
  TotalFile As Long
  FileName() As String
End Type

' 指定ディレクトリ情報
Public gDirInfo() As tDirInfo

' 処理対象のファイル数
Public gTotalFileNum As Long
' 処理済みのファイル数
Public gProcessedFileNum As Long


'*******************************************************************************
' 「集計開始」ボタンをクリック
'
' [引数]
' なし
'
' [戻り値]
' なし
'
' [処理概要]
' (Step 1)集計ディレクトリを記載する最終行を求め、指定ディレクトリ数を算出する。
'         (指定ディレクトリ数が0の場合、強制終了)
' (Step 2)指定ディレクトリが存在するかをチェック。
'         (存在しない場合、強制終了)
' (Step 3)指定ディレクトリ配下において、ファイル数、ファイル名を取得する。
' (Step 4)指定ディレクトリ配下のファイル数分、ファイルを読込み、各ファイルの
'         集計結果をマージしていく。
'         ※(Step 2〜4)の処理を指定ディレクトリ数分行う。
' (Step 5)全ファイルの集計をマージした結果を、「Test Data」シートへコピーする。
'*******************************************************************************
Public Sub btn_TestResultCollection()
Attribute btn_TestResultCollection.VB_ProcData.VB_Invoke_Func = " \n14"
  Const colDirSpec As Long = 4
  Const rowBegin As Long = 2
  Dim StartTime As Double
  Dim EndTime As Double
  Dim TotalTime As Double
  Dim rowLast As Long
  Dim numOfDir As Long
  Dim i As Long
  Dim wsMain As Worksheet: Set wsMain = Worksheets(gAggregationSheet)

  ' 開始時間取得
  StartTime = Timer

  ' プログレスバーFormを表示
  Info.Show vbModeless

  Application.ScreenUpdating = False
  ' Templateシートを再表示する
  Worksheets(gTemplateSheet).Visible = True

  ' (Step 1)集計ディレクトリを記載する最終行を求め、指定ディレクトリ数を算出する。
  '         (指定ディレクトリ数が0の場合、強制終了)
  ' ディレクトリが記載してある列の最終行を取得
  rowLast = wsMain.Cells(Rows.Count, colDirSpec).End(xlUp).row
  ' ディレクトリ数を算出
  numOfDir = rowLast - rowBegin

#If (cnsTest = 1) Then
  Debug.Print "(btn_TestResultCollection):rowLast = " & rowLast & _
              " numOfDir = " & numOfDir
#End If

  If (numOfDir = 0) Then
    MsgBox "[強制終了]集計ディレクトリが１つも指定されていません", vbCritical
    ' プログレスバーFormを閉じる
    Unload Info
    Exit Sub
  End If

  ReDim gDirInfo(numOfDir)

  ' 集計ディレクトリパスを収集
  For i = 1 To numOfDir
    gDirInfo(i).FullPath = wsMain.Cells(rowBegin + i, colDirSpec).Value
#If (cnsTest = 1) Then
    Debug.Print "  gDirInfo(" & i & ").FullPath = " & gDirInfo(i).FullPath
#End If
  Next i

  Call DispElapsedTime(StartTime, _
                       "Calculation of number of directories Complete : ")
  gTotalFileNum = 0
  gProcessedFileNum = 0

  For i = 1 To UBound(gDirInfo)
    ' (Step 2)指定ディレクトリが存在するかをチェック。
    '         (存在しない場合、強制終了)
    If (isExistDir(gDirInfo(i).FullPath) = False) Then
      MsgBox "[強制終了]指定ディレクトリが存在しません" & vbCrLf & _
             "(" & gDirInfo(i).FullPath & ")", vbCritical
      ' プログレスバーFormを閉じる
      Unload Info
      Exit Sub
    End If
    ' (Step 3)指定ディレクトリ配下において、ファイル数、ファイル名を取得する。
    Call GetFileNameAndNum(gDirInfo, i)
    ' 処理対象の全ファイル数を算出
    gTotalFileNum = gTotalFileNum + gDirInfo(i).TotalFile
  Next i

  Call DispElapsedTime(StartTime, _
                       "Calculation of number of files Complete : ")
  
#If (cnsTest = 1) Then
  Debug.Print "(btn_TestResultCollection):gTotalFileNum = " & gTotalFileNum
#End If
  ' 結果記入シートを新規作成
  Call CreateResultSheet

  ' (Step 4)指定ディレクトリ配下のファイル数分、ファイルを読込み、各ファイルの
  '         集計結果をマージしていく。
  ' 最初に現マージシートを新規作成しておく
  Call CreateSheetFromTemplate(gTemplateSheet, gMergeSheet)

  For i = 1 To UBound(gDirInfo)
    If (AggregatedByDir(gDirInfo, i) = False) Then
      Debug.Print "(btn_TestResultCollection):AggregatedByDir Fail!"
    End If

    Call DispElapsedTime(StartTime, _
                         "Directory(" & i & ") processing Complete : ")
  Next i

  ' 結果記入シートの体裁を整える
  Call ArrangeResultSheet

  ' 最後に読込んだ項目書データ出力シートを削除
  Application.DisplayAlerts = False
  Worksheets(gItemSheet).Delete
  Application.DisplayAlerts = True

  Call DispElapsedTime(StartTime, "All process Complete：")

  Application.ScreenUpdating = True
  ' Templateシートを非表示する
  Worksheets(gTemplateSheet).Visible = False

  ' プログレスバーFormを閉じる
  Unload Info

  ' 終了時間取得
  EndTime = Timer
  TotalTime = EndTime - StartTime

  MsgBox "Processing Complete!" & vbCrLf & _
         "elapsed time : " & TotalTime, vbInformation

End Sub

' 指定ディレクトリが存在するかチェックするFunction
' [引数]
'   FolderPath As String：フォルダ名(FullPath)
'
' [戻り値]
'   Boolean：True  (指定フォルダが存在する)
'          ：False (指定フォルダが存在しない)
Public Function isExistDir(ByVal FolderPath As String) As Boolean
  Dim result

#If (cnsTest = 1) Then
  Debug.Print "(isExistDir):FolderPath = " & FolderPath
#End If

  result = Dir(FolderPath, vbDirectory)
  If result = "" Then
    ' フォルダが存在しない
    isExistDir = False
  Else
    ' フォルダが存在する
    isExistDir = True
  End If

End Function

' 指定ディレクトリ内のファイル名、ファイル数を取得するFunction
' [引数]
'   DirInfo() As ｔDirInfo：指定ディレクトリ情報
'   DirIndex As Long：指定ディレクトリ情報のインデックス番号
'
' [戻り値]
'   なし
Public Function GetFileNameAndNum(ByRef DirInfo() As tDirInfo, _
                                  ByVal DirIndex As Long)
  Dim tmpPath As String
  Dim tmpFile As String
  Dim tmpCnt As Long
  Dim i As Long


  tmpPath = DirInfo(DirIndex).FullPath & "\*.xlsm"
#If (cnsTest = 1) Then
  Debug.Print "(GetFileNameAndNum):DirIndex = " & DirIndex & _
              " tmpPath = " & tmpPath
#End If

  tmpFile = Dir(tmpPath)
  Do While tmpFile <> ""
    tmpCnt = tmpCnt + 1

    ' 合計ファイル数を更新
    DirInfo(DirIndex).TotalFile = tmpCnt
    ' ファイル名を保存
    ReDim Preserve DirInfo(DirIndex).FileName(tmpCnt)
    DirInfo(DirIndex).FileName(tmpCnt) = tmpFile

    tmpFile = Dir()
  Loop

#If (cnsTest = 1) Then
  Debug.Print "(GetFileNameAndNum):tmpCnt = " & tmpCnt
#End If

  If (tmpCnt > 0) Then
    For i = 1 To tmpCnt
#If (cnsTest = 1) Then
      Debug.Print "  DirInfo(" & DirIndex & ").Filename(" & i & ") = " & _
                  DirInfo(DirIndex).FileName(i)
#End If
    Next i
  End If

End Function

' 指定テンプレートシートから指定シート名で新規シート作成するFunction
' [引数]
'   TemplateSheet As String：テンプレートとなるシート名
'   NewSheet As String：テンプレートを基に新規作成するシート名
'
' [戻り値]
'   なし
Private Function CreateSheetFromTemplate(ByVal TemplateSheet As String, _
                                         ByVal NewSheet As String)
  Dim wsOrg As Worksheet
  Dim ws As Worksheet

#If (cnsTest = 1) Then
  Debug.Print "(CreateSheetFromTemplate):TemplateSheet = " & TemplateSheet & _
              " NewSheet = " & NewSheet
#End If
  ' 基のシートを保存しておく
  Set wsOrg = ActiveSheet

  ' 指定シート名と同名のシートが有る場合、削除
  For Each ws In Worksheets
    If (ws.Name = NewSheet) Then
      Application.DisplayAlerts = False
      ws.Delete
      Application.DisplayAlerts = True
    End If
  Next ws
  
  ' 指定テンプレートシートを末尾にコピー
  Worksheets(TemplateSheet).Copy After:=Worksheets(Worksheets.Count)
  ' 指定シート名に変更
  ActiveSheet.Name = NewSheet

  ' 基のシートに切替
  wsOrg.Activate

End Function

' 指定ディレクトリ内の項目書ファイルから集計結果をマージするFunction
'
' [処理概要]
'   ※以下処理を、指定ディレクトリの全ファイルにおいて行う。
'   (Step 1)指定ディレクトリ/指定ファイルの集計シートからデータを読込んで
'           シートに出力。
'   (Step 2)現マージデータと(Step 1)で出力されたシートをマージして、
'           新マージシートに出力する。
'   (Step 3)現マージシートを削除し、新マージシート名を現マージシート名
'           に変更する。
' [引数]
'   DirInfo() As ｔDirInfo：指定ディレクトリ情報
'   DirIndex As Long：指定ディレクトリ情報のインデックス番号
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Private Function AggregatedByDir(ByRef DirInfo() As tDirInfo, _
                                 ByVal DirIndex As Long) As Boolean
  Dim wsItem As Worksheet
  Dim tempPath As String
  Dim tempFile As String
  Dim Target As String
  Dim numOfFile As Long
  Dim i As Long
  Dim ProgressRate As Long
  Dim strErrMsg As String

#If (cnsTest = 1) Then
  Debug.Print "(AggregatedByDir):DirIndex = " & DirIndex
#End If
  tempPath = DirInfo(DirIndex).FullPath
  numOfFile = DirInfo(DirIndex).TotalFile
#If (cnsTest = 1) Then
  Debug.Print "(AggregatedByDir):tempPath = " & tempPath & _
              " numOfFile = " & numOfFile
#End If

  ' ディレクトリ配下にファイルが無い場合、即時終了
  If (numOfFile = 0) Then
#If (cnsTest = 1) Then
    Debug.Print "(AggregatedByDir):There is no file under the directory."
#End If
    AggregatedByDir = False
    Exit Function
  End If

  ' ファイル数分、繰り返し
  For i = 1 To numOfFile
    ' (Step 1)指定ディレクトリ/指定ファイルの集計シートからデータを読込んで
    '         シートに出力。
    If (ReadItemData(DirInfo, DirIndex, i) = False) Then
      ' エラーの場合、次のファイル処理へ移る
      Exit For
    End If

    ' (Step 2)現マージデータと(Step 1)で出力されたシートをマージして、
    '         新マージシートに出力する。
    ' 一時集計マージ用シートを作成
    Call CreateSheetFromTemplate(gTemplateSheet, gTempSheet)

    ' 項目書データと現マージデータをマージした結果を、
    ' 一時集計マージ用シートに出力する。
    If (Merge2SheetTo1(gItemSheet, gMergeSheet, gTempSheet) = False) Then
      strErrMsg = "該当ファイルのマージに失敗しました。"
      ' 結果記入シートに結果(NG)を記入
      Call WriteResultNG(DirIndex, i, strErrMsg)

      ' エラーの場合、次のファイル処理へ移る
      Exit For
    End If

    ' (Step 3)現マージシートを削除し、新マージシート名を現マージシート名
    '         に変更する。
    Application.DisplayAlerts = False
    Worksheets(gMergeSheet).Delete
    Application.DisplayAlerts = True
    ' シート名変更
    Worksheets(gTempSheet).Name = gMergeSheet

    ' 結果記入シートに結果(OK)を記入
    Call WriteResultOK(DirIndex, i)

    ' 処理済みのファイル数をインクリメント
    gProcessedFileNum = gProcessedFileNum + 1
    ProgressRate = (gProcessedFileNum / gTotalFileNum) * 100
    Call UpdateProgressBar(ProgressRate)
  Next i

  AggregatedByDir = True

End Function

' 項目書ファイルのデータを読込んでシートに出力するFunction
' [引数]
'   DirInfo() As ｔDirInfo：指定ディレクトリ情報
'   DirIndex As Long：指定ディレクトリ情報のインデックス番号
'   FileIndex As Long：指定ファイル情報のインデックス番号
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Private Function ReadItemData(ByRef DirInfo() As tDirInfo, _
                              ByVal DirIndex As Long, _
                              ByVal FileIndex As Long) As Boolean
  Const colBegin As Long = 2
  Dim wb As Workbook
  Dim wbOrg As Workbook
  Dim wbItem As Workbook
  Dim wsOrg As Worksheet
  Dim wsItem As Worksheet
  Dim tempPath As String
  Dim tempFile As String
  Dim Target As String
  Dim colEnd As Long
  Dim rowNum As Long
  Dim colNum As Long
  Dim colBreak As Long
  Dim strBuf As String
  Dim strErrMsg As String
  Dim vTestData As Variant


  ' テンプレートシートを基に新規シート作成
  Call CreateSheetFromTemplate(gTemplateSheet, gItemSheet)

  Set wbOrg = ThisWorkbook
  Set wsOrg = ThisWorkbook.ActiveSheet
  Set wsItem = ThisWorkbook.Worksheets(gItemSheet)

  ' ディレクトリパス取得
  tempPath = DirInfo(DirIndex).FullPath
  ' ファイル名取得
  tempFile = DirInfo(DirIndex).FileName(FileIndex)
#If (cnsTest = 1) Then
  Debug.Print "(ReadItemData):tempPath = " & tempPath & _
              " tempFile = " & tempFile
#End If

  ' 同名ファイルが既に開かれている場合、エラー終了
  For Each wb In Workbooks
    If wb.Name = tempFile Then
      strErrMsg = "該当ファイルが既に開かれていたため、読み込みに失敗しました。"
      ' 結果記入シートに結果(NG)を記入
      Call WriteResultNG(DirIndex, FileIndex, strErrMsg)

      ReadItemData = False
      Exit Function
    End If
  Next wb

  Target = tempPath & "\" & tempFile

  ' 開いた項目書ブックを記憶
  Set wbItem = Workbooks.Open(Target)

  ' 項目書ブックOpen時にマクロ動作し、Trueに戻されるので再びFalseに設定
  Application.ScreenUpdating = False

  With wbItem.Sheets(gEachItemSheet)
    ' 最大列を求める
    colEnd = .Cells(eRowItems.Date, Columns.Count).End(xlToLeft).Column
#If (cnsTest = 1) Then
    Debug.Print "(ReadItemData):colEnd = " & colEnd
#End If
    ' 開いたブックの2列目〜最終列までの全テストデータを一旦配列に入れる
    vTestData = .Range(.Cells(eRowItems.Date, colBegin), _
                       .Cells(eRowItems.DailyBug, colEnd))
  End With

  ' 開いたブックの2列目〜最終列まで全データを出力
  With ThisWorkbook.Worksheets(gItemSheet)
    .Range(.Cells(eRowItems.Date, colBegin), _
           .Cells(eRowItems.DailyBug, colEnd)) = vTestData
  End With

  Application.DisplayAlerts = False
  ' 項目書ブックを閉じる
  wbItem.Close False
  Application.DisplayAlerts = True

  With wsItem
    .Activate
    ' Date行の書式を日付形式に設定
    .Range(.Cells(eRowItems.Date, colBegin), _
           .Cells(eRowItems.Date, colEnd)).NumberFormatLocal = "yyyy/mm/dd"
    ' 罫線を引く
    .Range(.Cells(eRowItems.Date, colBegin), _
           .Cells(eRowItems.DailyBug, colEnd)).Borders.LineStyle = xlContinuous
  End With

  ' 元のシートに切替
  wsOrg.Activate

  ReadItemData = True

End Function

' 指定された２シートのデータをマージして、指定シートに出力するFunction
' [引数]
'   inSheetA As String：マージ対象シート名A
'   inSheetB As String：マージ対象シート名B
'   outSheet As String：マージ結果出力シート名
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Private Function Merge2SheetTo1(ByVal inSheetA As String, _
                                ByVal inSheetB As String, _
                                ByVal outSheet As String) As Boolean
  Const colBegin As Long = 2
  Dim wsOrg As Worksheet
  Dim wsInA As Worksheet
  Dim wsInB As Worksheet
  Dim rowNum As Long
  Dim colNum As Long
  Dim colInA As Long, colInB As Long
  Dim colOut As Long
  Dim colEnd As Long
  Dim minDateA As Date, minDateB As Date
  Dim maxDateA As Date, maxDateB As Date
  Dim minDate As Date, maxDate As Date
  Dim colMaxA As Long, colMaxB As Long
  Dim colMergeStart As Long
  Dim skipFlagA As Boolean, skipFlagB As Boolean
  Dim vData() As Variant

#If (cnsTest = 1) Then
  Debug.Print "(Merge2SheetTo1):inSheetA = " & inSheetA & _
              " inSheetB = " & inSheetB & _
              " outSheet = " & outSheet
#End If

  Set wsOrg = ActiveSheet
  Set wsInA = Worksheets(inSheetA)
  Set wsInB = Worksheets(inSheetB)
  skipFlagA = False
  skipFlagB = False

  ' それぞれのシートの最小、最大日付を求める
  Call CalcMinMaxDate(inSheetA, minDateA, maxDateA)
  Call CalcMinMaxDate(inSheetB, minDateB, maxDateB)
  ' 最小日付を判定
  ' A <= Bの場合
  If (minDateA <= minDateB) Then
    minDate = minDateA
  ' 最小日付が0の場合、日付が未入力とみなす。
  ElseIf (minDateA = 0) And (minDateB <> 0) Then
    minDate = minDateB
    skipFlagA = True
  ElseIf (minDateB = 0) And (minDateA <> 0) Then
    minDate = minDateA
    skipFlagB = True
  ' 両シート共に日付が未入力の場合、エラー(１番最初の項目書の場合のみ)
  ElseIf (minDateA = 0) And (minDateB = 0) Then
    Merge2SheetTo1 = False
    Exit Function
  ' A > Bの場合
  Else
    minDate = minDateB
  End If

  ' 最大日付を判定
  If (maxDateA >= maxDateB) Then
    maxDate = maxDateA
  Else
    maxDate = maxDateB
  End If

#If (cnsTest = 1) Then
  Debug.Print "(Merge2SheetTo1):minDate = " & minDate & _
              " maxDate = " & maxDate
#End If
  ' 最大/最小日付より、期間を算出
  colEnd = colBegin + (maxDate - minDate)

#If (cnsTest = 1) Then
  Debug.Print "(Merge2SheetTo1):colEnd = " & colEnd
#End If

  ' マージ結果出力用シートへの出力用配列に、始めに算出した期間の日付のみ入力する
  ReDim vData(eRowItems.Date To eRowItems.DailyBug, colBegin To colEnd)
  ' 行
  For rowNum = eRowItems.Date To eRowItems.DailyBug
    ' 列
    For colNum = colBegin To colEnd
      Select Case rowNum
      ' Date行
      Case eRowItems.Date
        ' 最初に最小日付を設定
        If (colNum = colBegin) Then
          vData(rowNum, colNum) = minDate
        Else
          vData(rowNum, colNum) = vData(rowNum, colNum - 1) + 1
        End If

      ' 累計対象の項目
      Case Else
        ' 最初に0を入れておく
        vData(rowNum, colNum) = 0

      End Select
    Next colNum
  Next rowNum

  If (skipFlagA = True) Then
#If (cnsTest = 1) Then
    Debug.Print "(Merge2SheetTo1):skipFlagA = " & skipFlagA
#End If
    GoTo SKIP_SHEET_A
  End If

  ' マージ対象シートAより、日付が同じデータをコピーする
  colMaxA = isMaxCol(inSheetA, eRowItems.Date)
  colMergeStart = colEnd
  ' 行
  For rowNum = eRowItems.Date To eRowItems.DailyBug

    colInA = colBegin

    ' マージ結果出力シート列
    For colOut = colBegin To colEnd
      Select Case rowNum
      ' Date
      Case eRowItems.Date
        ' マージ対象シートの最初の日付が一致した列を保存しておく
        If (vData(rowNum, colOut) = _
            wsInA.Cells(rowNum, colBegin).Value) Then
          colMergeStart = colOut
#If (cnsTest = 1) Then
          Debug.Print "(Merge2SheetTo1):colMergeStart[A] = " & colMergeStart
#End If
        End If
      ' Total Tests Planned
      Case eRowItems.TotalTestsPlanned
        ' 全期間同じ値を設定する
        vData(rowNum, colOut) = vData(rowNum, colOut) + _
                                wsInA.Cells(rowNum, colBegin).Value

      Case Else
        ' 日付が一致した列以降のデータをマージする
        If (colOut >= colMergeStart) Then
          ' マージ元データの最終行までマージする
          If (colInA <= colMaxA) Then
            vData(rowNum, colOut) = vData(rowNum, colOut) + _
                                    wsInA.Cells(rowNum, colInA).Value
            colInA = colInA + 1
          ' マージ元データの最終行以降は、マージ元データの最後の値を足し込む
          Else
            vData(rowNum, colOut) = vData(rowNum, colOut) + _
                                    wsInA.Cells(rowNum, colMaxA).Value
          End If
        End If
      End Select

    Next colOut
  Next rowNum

SKIP_SHEET_A:

  If (skipFlagB = True) Then
#If (cnsTest = 1) Then
    Debug.Print "(Merge2SheetTo1):skipFlagB = " & skipFlagB
#End If
    GoTo SKIP_SHEET_B
  End If

  ' マージ対象シートBより、日付が同じデータをマージする
  colMaxB = isMaxCol(inSheetB, eRowItems.Date)
  colMergeStart = colEnd
  ' 行
  For rowNum = eRowItems.Date To eRowItems.DailyBug

    colInB = colBegin

    ' マージ結果出力シート列
    For colOut = colBegin To colEnd
      Select Case rowNum
      ' Date
      Case eRowItems.Date
        If (vData(rowNum, colOut) = _
            wsInB.Cells(rowNum, colBegin).Value) Then
          colMergeStart = colOut
#If (cnsTest = 1) Then
          Debug.Print "(Merge2SheetTo1):colMergeStart[B] = " & colMergeStart
#End If
        End If
      ' Total Tests Planned
      Case eRowItems.TotalTestsPlanned
        ' 全期間同じ値を設定する
        vData(rowNum, colOut) = vData(rowNum, colOut) + _
                                wsInB.Cells(rowNum, colBegin).Value

      Case Else
        ' 日付が一致した列以降のデータをマージする
        If (colOut >= colMergeStart) Then
          ' マージ元データの最終行までマージする
          If (colInB <= colMaxB) Then
            vData(rowNum, colOut) = vData(rowNum, colOut) + _
                                    wsInB.Cells(rowNum, colInB).Value
            colInB = colInB + 1
          ' マージ元データの最終行以降は、マージ元データの最後の値を足し込む
          Else
            vData(rowNum, colOut) = vData(rowNum, colOut) + _
                                    wsInB.Cells(rowNum, colMaxB).Value
          End If
        End If
      End Select

    Next colOut
  Next rowNum

SKIP_SHEET_B:

  With Worksheets(outSheet)
    ' セルに一括代入
    .Range(.Cells(eRowItems.Date, colBegin), _
           .Cells(eRowItems.DailyBug, colEnd)) = vData
    ' 罫線を引く
    .Range(.Cells(eRowItems.Date, colBegin), _
           .Cells(eRowItems.DailyBug, colEnd)).Borders.LineStyle = xlContinuous
  End With

  ' 元のシートに切替
  wsOrg.Activate

  Merge2SheetTo1 = True

End Function

' 指定シートの最大、最小日付を求めるFunction
' [引数]
'   SheetName As String：シート名
'   minDate As String：最小日付
'   maxDate As String：最大日付
'
' [戻り値]
'   なし
Private Function CalcMinMaxDate(ByVal SheetName As String, _
                                ByRef minDate As Date, _
                                ByRef maxDate As Date)
  Const colBegin As Long = 2
  Dim colEnd As Long
  Dim wsOrg As Worksheet
  Dim ws As Worksheet

#If (cnsTest = 1) Then
  Debug.Print "(CalcMinMaxDate):SheetName = " & SheetName
#End If

  Set wsOrg = ActiveSheet
  Set ws = Worksheets(SheetName)
  ' 指定シートに切替
  ws.Activate
  ' Date行の最終列を求める
  colEnd = Cells(eRowItems.Date, Columns.Count).End(xlToLeft).Column

  With Application.WorksheetFunction
    minDate = .Min(Range(Cells(eRowItems.Date, colBegin), _
                         Cells(eRowItems.Date, colEnd)))
    maxDate = .Max(Range(Cells(eRowItems.Date, colBegin), _
                         Cells(eRowItems.Date, colEnd)))
  End With

#If (cnsTest = 1) Then
  Debug.Print "(CalcMinMaxDate):minDate = " & minDate & _
              " maxDate = " & maxDate
#End If

  ' 元のシートに切替
  wsOrg.Activate

End Function

' 指定シート/行の最大列を求めるFunction
' [引数]
'   SheetName As String：シート名
'   rowNum As Long：行数
'
' [戻り値]
'   Long：指定行の最大列数
Private Function isMaxCol(ByVal SheetName As String, _
                          ByVal rowNum As Long) As Long
  Dim wsOrg As Worksheet
  Dim ws As Worksheet
  Dim colMax As Long

#If (cnsTest = 1) Then
  Debug.Print "(isMaxCol):SheetName = " & SheetName & _
              " rowNum = " & rowNum
#End If
  Set wsOrg = ActiveSheet
  Set ws = Worksheets(SheetName)

  ' 指定シートに切替
  ws.Activate
  colMax = Cells(rowNum, Columns.Count).End(xlToLeft).Column
#If (cnsTest = 1) Then
  Debug.Print "(isMaxCol):colMax = " & colMax
#End If

  ' 元のシートに切替
  wsOrg.Activate
  isMaxCol = colMax

End Function

' プログレスバーの処理進捗率を更新するFunction
' [引数]
'   percent As Long：プログレスバーに表示する％値
'
' [戻り値]
'   なし
Public Function UpdateProgressBar(ByVal percent As Long)
  With Info
    .ProgressBar1.Value = percent
    .percent.Caption = Int(percent / .ProgressBar1.Max * 100) & "%"
    .Repaint
  End With
End Function

' 途中までの経過時間をデバッグ表示するFunction
' [引数]
'   StartTime As Double ：処理開始時間
'   strDispMsg As String：経過時間の前に付与するラベル文字列
'
' [戻り値]
'   なし
Public Function DispElapsedTime(ByVal StartTime As Double, _
                                ByVal strDispMsg As String)
  Dim MiddleTime As Double
  Dim ProcessTime As Double

  MiddleTime = Timer
  ProcessTime = MiddleTime - StartTime
  Debug.Print strDispMsg; ProcessTime

End Function


