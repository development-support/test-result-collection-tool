Attribute VB_Name = "Copy2TestData"
Option Explicit

' 下記の2行の内のどちらかをコメントにして下さい！
#Const cnsTest = 0  ' ←本番
'#Const cnsTest = 1   ' ←テスト

' Test Dataシートの項目行
Public Enum eTestData
  Date = 2
  TotalTestsPlanned
  OriginalPlannedTests
  EstimatedDailyPassRate
  PlannedAttempts
  PlannedTests
  PlannedPass
  ActualAttempts
  Passed
  Blocked
  Failed
  FailedReady4ReTest
  ReadyNotAttempted
  Executed
  perExecuted
  perPassed
  CumulativePassRate
  DailyPassRate
  PTRsUnResolved
  PTRsResolved
  PTRsSubmittedDaily
  EstimatedCumulativePassRate
  PhaseSpecificData
  DailyBug
End Enum


'*******************************************************************************
' 「集計結果をTest Dataへコピー」ボタンをクリック
'
' [引数]
' なし
'
' [戻り値]
' なし
'
' [処理概要]
' (Step 1)MergeDataシートが存在するかチェックする。
'         (存在しない場合、強制終了)
' (Step 2)Test Dataの入力必須のデータを全クリアする。
' (Step 3)MergeDataシートの内容から必要なデータをTest Dataへコピーする。
'*******************************************************************************
Public Sub btn_Copy2TestData()
  Dim StartTime As Double
  Dim EndTime As Double
  Dim TotalTime As Double

  ' 開始時間取得
  StartTime = Timer

  Application.ScreenUpdating = False

  ' (Step 1)MergeDataシートが存在するかチェックする。
  '         (存在しない場合、強制終了)
  If (isSheetExist(gMergeSheet) = False) Then
    MsgBox "[強制終了]" & gMergeSheet & "シートがありません" & vbCrLf & _
           "最初に「集計開始」ボタンを押してください", vbCritical
    Exit Sub
  End If

  ' (Step 2)Test Dataの入力必須のデータを全クリアする。
  Call ClearTestData

  Call DispElapsedTime(StartTime, "ClearTestData Complete：")

  ' (Step 3)MergeDataシートの内容から必要なデータをTest Dataへコピーする。
  Call Copy2TestData(gMergeSheet)

  Call DispElapsedTime(StartTime, "Copy2TestData Complete：")

  Application.ScreenUpdating = True

  ' 終了時間取得
  EndTime = Timer
  TotalTime = EndTime - StartTime

  MsgBox "Copy Complete!" & vbCrLf & _
         "elapsed time : " & TotalTime, vbInformation

End Sub

' 指定シートが存在するかチェックするFunction
' [引数]
'   strSheetName As String：シート名
'
' [戻り値]
'   Boolean：True  (指定シートが存在する)
'          ：False (指定シートが存在しない)
Public Function isSheetExist(ByVal strSheetName As String) As Boolean
  Dim ws As Worksheet
  Dim FindFlg As Boolean

  FindFlg = False

  For Each ws In Worksheets
    If ws.Name = strSheetName Then
      FindFlg = True
      Exit For
    End If
  Next ws

  If FindFlg = False Then
    isSheetExist = False
    Exit Function
  End If

  isSheetExist = True

End Function

' Test DataのデータをクリアするFunction
' [引数]
'   なし
'
' [戻り値]
'   なし
Public Function ClearTestData()
  Const colBegin As Long = 2
  Dim wsOrg As Worksheet
  Dim wsTD As Worksheet
  Dim colEnd As Long
  Dim i As Long
  Dim j As Long


  Set wsOrg = ActiveSheet
  Set wsTD = Worksheets(gTestData)
  ' Test Dataシートに切替
  wsTD.Activate

  ' 最終列取得
  colEnd = Cells(eTestData.TotalTestsPlanned, Columns.Count).End(xlToLeft).Column
#If (cnsTest = 1) Then
  Debug.Print "(ClearTestData):colEnd = " & colEnd
#End If

  With wsTD
    ' 行：Total Tests Planned〜Ready (Not Attempted)
    .Range(.Cells(eTestData.TotalTestsPlanned, colBegin), _
           .Cells(eTestData.ReadyNotAttempted, colEnd)).ClearContents
    ' 行：CR/MJ PTRs UnResolved〜CR/MJ PTRs Submitted Daily
    .Range(.Cells(eTestData.PTRsUnResolved, colBegin), _
           .Cells(eTestData.PTRsSubmittedDaily, colEnd)).ClearContents
    ' 行：Daily Bug
    .Range(.Cells(eTestData.DailyBug, colBegin), _
           .Cells(eTestData.DailyBug, colEnd)).ClearContents
  End With

  ' 元のシートに切替
  wsOrg.Activate

End Function

' 指定シートの内容から必要なデータをTest DataへコピーするFunction
' [引数]
'   strSheetName As String：シート名
'
' [戻り値]
'   なし
Public Function Copy2TestData(ByVal strSheetName As String)
  Const colBegin As Long = 2
  Dim wsOrg As Worksheet
  Dim wsMerge As Worksheet
  Dim wsTD As Worksheet
  Dim colEnd As Long
  Dim i As Long
  Dim j As Long
  Dim minDate As Date
  Dim maxDate As Date
  Dim rowStartReportDate As Long
  Dim rowFinalReportDate As Long
  Dim rowCurrentReportDate As Long
  Dim curPlannedPass As Long
  Dim curPlannedAttempts As Long
  Dim prePlannedPass As Long
  Dim prePlannedAttempts As Long
  Dim vData() As Variant

#If (cnsTest = 1) Then
  Debug.Print "(Copy2TestData):strSheetName = " & strSheetName
#End If
  Set wsOrg = ActiveSheet
  Set wsMerge = Worksheets(strSheetName)
  Set wsTD = Worksheets(gTestData)

  ' 指定シートへ切替
  wsMerge.Activate
  ' 最終列取得
  colEnd = Cells(eRowItems.Date, Columns.Count).End(xlToLeft).Column
  ' 最小、最大日付を取得
  minDate = Cells(eRowItems.Date, colBegin).Value
  maxDate = Cells(eRowItems.Date, colEnd).Value
#If (cnsTest = 1) Then
  Debug.Print "(Copy2TestData):minDate = " & minDate & _
              " maxDate = " & maxDate
#End If

  ' Test Dataシートへ切替
  wsTD.Activate
  rowStartReportDate = GetTargetStringRow(Columns(1), "Start Report Date", True)
  rowFinalReportDate = GetTargetStringRow(Columns(1), "Final Report Date", True)
  rowCurrentReportDate = GetTargetStringRow(Columns(1), "Current Report Date", True)
#If (cnsTest = 1) Then
  Debug.Print "(Copy2TestData):rowStartReportDate = " & rowStartReportDate & _
              " rowFinalReportDate = " & rowFinalReportDate & _
              " rowCurrentReportDate = " & rowCurrentReportDate
#End If

  ' Test Dataシートへ値を設定
  With Worksheets(gTestData)
    ' Start Report Date
    .Cells(rowStartReportDate + 1, 1).Value = minDate
    ' Final Report Date
    .Cells(rowFinalReportDate + 1, 1).Value = maxDate
    ' Current Report Date
    .Cells(rowCurrentReportDate + 1, 1).Value = maxDate
    ' 指定シートからデータをコピー
    ' **** Total Tests Planned〜Ready (Not Attempted) ****
    ReDim vData(eTestData.TotalTestsPlanned To eTestData.ReadyNotAttempted, _
                colBegin To colEnd)
    ' 行
    For i = eTestData.TotalTestsPlanned To eTestData.ReadyNotAttempted
      ' 列
      For j = colBegin To colEnd
        Select Case i
        ' Total Tests Planned
        Case eTestData.TotalTestsPlanned
          vData(i, j) = wsMerge.Cells(eRowItems.TotalTestsPlanned, j).Value
        ' Original Planned Tests
        Case eTestData.OriginalPlannedTests
          vData(i, j) = wsMerge.Cells(eRowItems.TotalTestsPlanned, j).Value
        ' Planned Attempts
        Case eTestData.PlannedAttempts
          vData(i, j) = wsMerge.Cells(eRowItems.PlannedTests, j).Value
        ' Planned Tests
        Case eTestData.PlannedTests
          vData(i, j) = wsMerge.Cells(eRowItems.PlannedTests, j).Value
        ' Planned Pass
        Case eTestData.PlannedPass
          vData(i, j) = wsMerge.Cells(eRowItems.PlannedPass, j).Value
        ' Actual Attempts
        Case eTestData.ActualAttempts
          vData(i, j) = wsMerge.Cells(eRowItems.ActualAttempts, j).Value
        ' Passed
        Case eTestData.Passed
          vData(i, j) = wsMerge.Cells(eRowItems.Passed, j).Value
        ' Blocked
        Case eTestData.Blocked
          vData(i, j) = wsMerge.Cells(eRowItems.Blocked, j).Value
        ' Failed
        Case eTestData.Failed
          vData(i, j) = wsMerge.Cells(eRowItems.Failed, j).Value
        ' Failed (Ready for ReTest)
        Case eTestData.FailedReady4ReTest
          vData(i, j) = wsMerge.Cells(eRowItems.FailedReady4ReTest, j).Value
        ' Ready (Not Attempted)
        Case eTestData.ReadyNotAttempted
          vData(i, j) = wsMerge.Cells(eRowItems.ReadyNotAttempted, j).Value
        End Select
      Next j
    Next i

    ' Total Tests Planned〜Ready (Not Attempted)までを一括出力
    .Range(.Cells(eTestData.TotalTestsPlanned, colBegin), _
           .Cells(eTestData.ReadyNotAttempted, colEnd)) = vData

    ' **** CR/MJ PTRs UnResolved〜CR/MJ PTRs Submitted Daily ****
    ReDim vData(eTestData.PTRsUnResolved To eTestData.PTRsSubmittedDaily, _
                colBegin To colEnd)
    ' 行
    For i = eTestData.PTRsUnResolved To eTestData.PTRsSubmittedDaily
      ' 列
      For j = colBegin To colEnd
        Select Case i
        ' CR/MJ PTRs UnResolved
        Case eTestData.PTRsUnResolved
          vData(i, j) = wsMerge.Cells(eRowItems.PTRsUnResolved, j).Value
        ' CR/MJ PTRs Resolved
        Case eTestData.PTRsResolved
          vData(i, j) = wsMerge.Cells(eRowItems.PTRsResolved, j).Value
        ' CR/MJ PTRs Submitted Daily
        Case eTestData.PTRsSubmittedDaily
          vData(i, j) = wsMerge.Cells(eRowItems.PTRsSubmittedDaily, j).Value
        End Select
      Next j
    Next i

    ' CR/MJ PTRs UnResolved〜CR/MJ PTRs Submitted Dailyまでを一括出力
    .Range(.Cells(eTestData.PTRsUnResolved, colBegin), _
           .Cells(eTestData.PTRsSubmittedDaily, colEnd)) = vData

    ' **** Daily Bug ****
    ReDim vData(colBegin To colEnd)

    ' Daily Bug
    For j = colBegin To colEnd
      vData(j) = wsMerge.Cells(eRowItems.DailyBug, j).Value
    Next j

    ' Daily Bugを一括出力
    .Range(.Cells(eTestData.DailyBug, colBegin), _
           .Cells(eTestData.DailyBug, colEnd)) = vData

    ' **** Estimated Daily Pass Rate ****
    ReDim vData(colBegin To colEnd)

    ' 列
    For j = colBegin To colEnd
      ' Estimated Daily Pass Rate
      curPlannedPass = .Cells(eTestData.PlannedPass, j).Value
      curPlannedAttempts = .Cells(eTestData.PlannedAttempts, j).Value

      If (j = colBegin) Then
        If (curPlannedAttempts = 0) Then
          vData(j) = 0
        Else
          vData(j) = curPlannedPass / curPlannedAttempts
        End If
      Else
        prePlannedPass = .Cells(eTestData.PlannedPass, j - 1).Value
        prePlannedAttempts = .Cells(eTestData.PlannedAttempts, j - 1).Value

        If ((curPlannedAttempts - prePlannedAttempts) = 0) Then
          vData(j) = 0
        Else
          vData(j) = (curPlannedPass - prePlannedPass) / (curPlannedAttempts - prePlannedAttempts)
        End If
      End If
    Next j

    ' Estimated Daily Pass Rateを一括出力
    .Range(.Cells(eTestData.EstimatedDailyPassRate, colBegin), _
           .Cells(eTestData.EstimatedDailyPassRate, colEnd)) = vData

  End With

  ' 元のシートに切替
  wsOrg.Activate

End Function

' 検索文字列の行番号を取得するFunction
' [引数]
'   rangeTarget As Range：検索範囲
'   strTargetString As String：検索文字列
'   boofull As Boolean：検索一致オプション
'              [default](False):部分一致
'                       (True) :全一致
'
' [戻り値]
'   Long：検索文字列が見つかった行番号
Public Function GetTargetStringRow(ByVal rangeTarget As Range, _
                                   ByVal strTargetString As String, _
                                   Optional ByVal boofull As Boolean = False) As Long
  Dim Ret As Long
  Dim Foundcell As Range

  If (boofull = True) Then
    Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                     LookIn:=xlValues, _
                                     LookAt:=xlWhole, _
                                     MatchCase:=False, _
                                     MatchByte:=False)
  Else
    Set Foundcell = rangeTarget.Find(what:=strTargetString, _
                                     LookIn:=xlValues, _
                                     LookAt:=xlPart, _
                                     MatchCase:=False, _
                                     MatchByte:=False)
  End If

  If Foundcell Is Nothing Then
    Ret = 0
  Else
    Ret = Foundcell.row
  End If

  GetTargetStringRow = Ret

End Function

