Attribute VB_Name = "WriteResult"
Option Explicit

' 下記の2行の内のどちらかをコメントにして下さい！
#Const cnsTest = 0  ' ←本番
'#Const cnsTest = 1   ' ←テスト

' 集計マージ結果記入用シート名
Public Const gMergeResult As String = "MergeResult"

Public Enum eHeadRst
  FolderPath = 1
  FileName
  ResultStatus
  CompRate
  NumOfItems
  NumOfTries
  NumOfOK
  NumOfNG
  NumOfBLK
  NumOfRemain
End Enum

' 現在の結果記入行
Public gRowResult As Long

' 結果記入シートを新規作成するFunction
' [引数]
'   なし
'
' [戻り値]
'   なし
Public Function CreateResultSheet()
  Const HeadingOfResult As String = "FolderPath,FileName,Result,完了率,項目数,トライ,OK,NG,BLK,残項目"
  Dim wsOrg As Worksheet
  Dim vHeading As Variant
  Dim i As Long


  Set wsOrg = ActiveSheet

  ' 同名シートが存在する場合、一旦削除
  If (isSheetExist(gMergeResult) = True) Then
    Application.DisplayAlerts = False
    Worksheets(gMergeResult).Delete
    Application.DisplayAlerts = True
  End If

  ' ワークシートの最後尾にシート追加
  Worksheets.Add After:=Worksheets(Worksheets.Count)
  ActiveSheet.Name = gMergeResult

  vHeading = Split(HeadingOfResult, ",")

#If (cnsTest = 1) Then
  For i = LBound(vHeading) To UBound(vHeading)
    Debug.Print "(CreateResultSheet):vHeading(" & i & ") = " & vHeading(i)
  Next i
#End If

  With Worksheets(gMergeResult)
    ' 見出し設定
    .Range(.Cells(1, eHeadRst.FolderPath), _
           .Cells(1, eHeadRst.NumOfRemain)) = vHeading

    ' 見出しのフォントスタイルをBoldに設定
    .Range(.Cells(1, eHeadRst.FolderPath), _
           .Cells(1, eHeadRst.NumOfRemain)).Font.Bold = True

    ' 見出しセルの背景色を設定
    .Range(.Cells(1, eHeadRst.FolderPath), _
           .Cells(1, eHeadRst.NumOfRemain)).Interior.Color = RGB(200, 255, 255)

    ' シート全体のフォント設定
    .Cells.Font.Name = "Meiryo UI"
  End With

  ' 初期値設定
  gRowResult = 2

  ' 元のシートに切替
  wsOrg.Activate

End Function

' 結果記入シートの体裁を整えるFunction
' [引数]
'   なし
'
' [戻り値]
'   なし
Public Function ArrangeResultSheet()
  Dim wsOrg As Worksheet
  Dim wsRst As Worksheet
  Dim rowEnd As Long
  Dim i As Long


  Set wsOrg = ActiveSheet
  Set wsRst = Worksheets(gMergeResult)

  ' 結果記入シートに切替
  With wsRst
    .Activate
    ' FileName列の最終行を求める
    rowEnd = .Cells(Rows.Count, eHeadRst.FileName).End(xlUp).row
#If (cnsTest = 1) Then
    Debug.Print "(ArrangeResultSheet):rowEnd = " & rowEnd
#End If
    ' 罫線を引く
    .Range(.Cells(1, eHeadRst.FolderPath), _
           .Cells(rowEnd, eHeadRst.NumOfRemain)).Borders.LineStyle = xlContinuous

    ' シート全体の列幅を自動調整する
    .Cells.EntireColumn.AutoFit

    ' 完了率セルの書式設定をパーセント(少数第1位まで)に設定
    .Range(Cells(2, eHeadRst.CompRate), _
           Cells(rowEnd, eHeadRst.CompRate)).NumberFormatLocal = "0.0%"

    ' 完了率セルの条件付き書式(データバー)を設定
    With .Range(.Cells(2, eHeadRst.CompRate), _
                .Cells(rowEnd, eHeadRst.CompRate)).FormatConditions.AddDatabar
      ' 最小値を指定範囲内の最小値に自動で比例させる
      .MinPoint.Modify newtype:=xlConditionValueAutomaticMin
      ' 最大値を指定範囲内の最大値に自動で比例させる
      .MaxPoint.Modify newtype:=xlConditionValueAutomaticMax
    End With

    ' 完了率〜残項目までの列幅を設定
    For i = eHeadRst.CompRate To eHeadRst.NumOfRemain
      .Columns(i).ColumnWidth = 10
    Next i
  End With

  ' 元のシートへ切替
  wsOrg.Activate

End Function

' 各項目書データのマージ結果(OK)を結果記入シートに記載するFunction
' [引数]
'   FolderIndex As Long：フォルダIndex番号
'   FileIndex As Long：ファイルIndex番号
'
' [戻り値]
'   なし
Public Function WriteResultOK(ByVal FolderIndex As Long, _
                              ByVal FileIndex As Long)
  Dim wsOrg As Worksheet
  Dim wsRst As Worksheet
  Dim wsItem As Worksheet
  Dim colItemEnd As Long
  Dim strCompRate As String
  Dim strRemain As String
  Dim vData(eHeadRst.FolderPath To eHeadRst.NumOfRemain) As Variant

#If (cnsTest = 1) Then
  Debug.Print "(WriteResultOK):FolderIndex = " & FolderIndex & _
              " FileIndex = " & FileIndex
#End If
  Set wsOrg = ActiveSheet
  Set wsRst = Worksheets(gMergeResult)
  Set wsItem = Worksheets(gItemSheet)

  ' 各項目書データ用シートに切替
  With wsItem
    .Activate

    colItemEnd = .Cells(eRowItems.Date, Columns.Count).End(xlToLeft).Column
  End With

  ' 結果記入シートの出力データを作成
  ' 最初のファイルの時のみフォルダを記載
  If (FileIndex = 1) Then
    vData(eHeadRst.FolderPath) = gDirInfo(FolderIndex).FullPath
  End If
  ' ファイル名を記載
  vData(eHeadRst.FileName) = gDirInfo(FolderIndex).FileName(FileIndex)
  ' 結果を記載
  vData(eHeadRst.ResultStatus) = "OK"
  ' 完了率を記載
  strCompRate = "=" & wsRst.Cells(gRowResult, eHeadRst.NumOfOK).Address(False, False) & _
                "/" & wsRst.Cells(gRowResult, eHeadRst.NumOfItems).Address(False, False)
#If (cnsTest = 1) Then
    Debug.Print "(WriteResultOK):strCompRate = " & strCompRate
#End If
  vData(eHeadRst.CompRate) = strCompRate
  ' 項目数を記載
  vData(eHeadRst.NumOfItems) = wsItem.Cells(eRowItems.TotalTestsPlanned, colItemEnd)
  ' トライを記載
  vData(eHeadRst.NumOfTries) = wsItem.Cells(eRowItems.ActualAttempts, colItemEnd)
  ' OKを記載
  vData(eHeadRst.NumOfOK) = wsItem.Cells(eRowItems.Passed, colItemEnd)
  ' NGを記載
  vData(eHeadRst.NumOfNG) = wsItem.Cells(eRowItems.Failed, colItemEnd)
  ' BLKを記載
  vData(eHeadRst.NumOfBLK) = wsItem.Cells(eRowItems.Blocked, colItemEnd)
  ' 残項目を記載
  strRemain = "=" & wsRst.Cells(gRowResult, eHeadRst.NumOfItems).Address(False, False) & _
              "-(" & wsRst.Cells(gRowResult, eHeadRst.NumOfOK).Address(False, False) & _
              "+" & wsRst.Cells(gRowResult, eHeadRst.NumOfNG).Address(False, False) & _
              "+" & wsRst.Cells(gRowResult, eHeadRst.NumOfBLK).Address(False, False) & ")"
#If (cnsTest = 1) Then
    Debug.Print "(WriteResultOK):strRemain = " & strRemain
#End If
  vData(eHeadRst.NumOfRemain) = strRemain

  ' 結果記入シートへ出力データを一括出力
  With Worksheets(gMergeResult)
    .Range(.Cells(gRowResult, eHeadRst.FolderPath), _
           .Cells(gRowResult, eHeadRst.NumOfRemain)) = vData
  End With

  ' 現在の結果記入行をインクリメント
  gRowResult = gRowResult + 1

  ' 元のシートへ切替
  wsOrg.Activate

End Function

' 各項目書データのマージ結果(NG)を結果記入シートに記載するFunction
' [引数]
'   FolderIndex As Long：フォルダIndex番号
'   FileIndex As Long：ファイルIndex番号
'
' [戻り値]
'   なし
Public Function WriteResultNG(ByVal FolderIndex As Long, _
                              ByVal FileIndex As Long, _
                              ByVal strResult As String)
  Dim wsOrg As Worksheet
  Dim vData(eHeadRst.FolderPath To eHeadRst.NumOfRemain) As Variant

#If (cnsTest = 1) Then
  Debug.Print "(WriteResultNG):FolderIndex = " & FolderIndex & _
              " FileIndex = " & FileIndex & vbCrLf & _
              "                strResult = " & strResult
#End If
  Set wsOrg = ActiveSheet

  ' 結果記入シートの出力データを作成
  ' 最初のファイルの時のみフォルダを記載
  If (FileIndex = 1) Then
    vData(eHeadRst.FolderPath).Value = gDirInfo(FolderIndex).FullPath
  End If
  ' ファイル名を記載
  vData(eHeadRst.FileName).Value = gDirInfo(FolderIndex).FileName(FileIndex)
  ' 結果を記載
  vData(eHeadRst.ResultStatus).Value = strResult

  ' 結果記入シートへ出力データを一括出力
  With Worksheets(gMergeResult)
    .Range(.Cells(gRowResult, eHeadRst.FolderPath), _
           .Cells(gRowResult, eHeadRst.NumOfRemain)) = vData
  End With

  ' 現在の結果記入行をインクリメント
  gRowResult = gRowResult + 1

  ' 元のシートへ切替
  wsOrg.Activate

End Function

