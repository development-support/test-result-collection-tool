Attribute VB_Name = "Module1"
Sub データ変換()

Dim LineNo As Integer
Dim SheetNo As Integer
Dim Line As Integer


LineNo = 6
SheetNo = 7
Line = 6
Worksheets(SheetNo).Select

Do While Cells(Line, 19) <> ""


'要因分類を変換

    Select Case Cells(Line, 19).Value
        Case "1)F001)ﾊｰﾄﾞ情報不備・不足"
            Sheets("CT問題処理票").Cells(LineNo, 19).Value = "F001)ﾊｰﾄﾞ情報不備・不足"
        Case "2)F002)ｶｰﾄﾞ間情報不備・不足"
            Sheets("CT問題処理票").Cells(LineNo, 19).Value = "F002)ｶｰﾄﾞ間情報不備・不足"
        Case "3)F003)方式誤り・仕様検討不足"
            Sheets("CT問題処理票").Cells(LineNo, 19).Value = "F003)方式誤り・仕様検討不足"
        Case "4)F004)仕様解釈ﾐｽ・仕様不明確"
            Sheets("CT問題処理票").Cells(LineNo, 19).Value = "F004)仕様解釈ﾐｽ・仕様不明確"
        Case "5)F005)仕様変更未通知"
            Sheets("CT問題処理票").Cells(LineNo, 19).Value = "F005)仕様変更未通知"
        Case "6)F006)担当者ｽｷﾙ不足"
            Sheets("CT問題処理票").Cells(LineNo, 19).Value = "F006)担当者ｽｷﾙ不足"
        Case "7)F007)設計書不備・不足"
            Sheets("CT問題処理票").Cells(LineNo, 19).Value = "F007)設計書不備・不足"
        Case "8)F008)規約不備・不足"
            Sheets("CT問題処理票").Cells(LineNo, 19).Value = "F008)規約不備・不足"
        Case "9)F009)論理誤り"
            Sheets("CT問題処理票").Cells(LineNo, 19).Value = "F009)論理誤り"
        Case "10)F010)単純ﾐｽ"
            Sheets("CT問題処理票").Cells(LineNo, 19).Value = "F010)単純ﾐｽ"
        Case "11)F011)APL情報不備・不足"
            Sheets("CT問題処理票").Cells(LineNo, 19).Value = "F011)APL情報不備・不足"
        Case "12)F012)影響範囲の調査・考慮不足"
            Sheets("CT問題処理票").Cells(LineNo, 19).Value = "F012)影響範囲の調査・考慮不足"
        Case "13)F099)その他"
            Sheets("CT問題処理票").Cells(LineNo, 19).Value = "F099)その他"
    End Select
   
'原因分類を変換
    Select Case Cells(Line, 22).Value
        Case "1)C010)ハート゛ソフト間インターフェース誤り"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C010)ハート゛ソフト間インターフェース誤り"
        Case "2)C020)APL 間インターフェース誤り"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C020)APL 間インターフェース誤り"
        Case "3)C030)カード間インターフェース誤り"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C030)カード間インターフェース誤り"
        Case "4)C040)サブシステム間インターフェース誤り"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C040)サブシステム間インターフェース誤り"
        Case "5)C050)タスク間インターフェース誤り"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C050)タスク間インターフェース誤り"
        Case "6)C060)機能分担誤り"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C060)機能分担誤り"
        Case "7)C070)リソース設計誤り"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C070)リソース設計誤り"
        Case "8)C080)性能設計誤り"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C080)性能設計誤り"
        Case "9)C090)機能漏れ"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C090)機能漏れ"
        Case "10)C100)競合誤り"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C100)競合誤り"
        Case "11)C110)制御誤り"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C110)制御誤り"
        Case "12)C120)ﾃﾞｰﾀ誤り(ﾃﾞｰﾀ構造/初期値等)"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C120)ﾃﾞｰﾀ誤り(ﾃﾞｰﾀ構造/初期値等)"
        Case "13)C130)異常処理考慮不足"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C130)異常処理考慮不足"
        Case "14)C140)記述漏れ/表現不足"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C140)記述漏れ/表現不足"
        Case "15)C150)ｺｰﾃﾞｨﾝｸﾞﾐｽ"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C150)ｺｰﾃﾞｨﾝｸﾞﾐｽ"
        Case "16)C160)試験項目過不足"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C160)試験項目過不足"
        Case "17)C170)試験手順誤り"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C170)試験手順誤り"
        Case "18)C180)試験環境誤り"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C180)試験環境誤り"
        Case "19)C190)修正設計誤り"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C190)修正設計誤り"
        Case "20)C200)修正コーディング誤り"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C200)修正コーディング誤り"
        Case "21)C799)上記分類以外のバグ"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C799)上記分類以外のバグ"
        Case "22)C800)仕様変更・追加(顧客)"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C800)仕様変更・追加(顧客)"
        Case "23)C810)仕様変更・追加（社内）"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C810)仕様変更・追加（社内）"
        Case "24)C820)仕様変更・追加（社内H/S）"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C820)仕様変更・追加（社内H/S）"
        Case "25)C830)性能改善"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C830)性能改善"
        Case "26)C840)操作性改善"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C840)操作性改善"
        Case "27)C850)処理改善（リファクタリング含む）"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C850)処理改善（リファクタリング含む）"
        Case "28)C860)その他改善"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C860)その他改善"
        Case "29)C870)誤字/脱字"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C870)誤字/脱字"
        Case "30)C999)上記分類以外の非バグ"
            Sheets("CT問題処理票").Cells(LineNo, 22).Value = "C999)上記分類以外の非バグ"
    End Select
    
     
'次のデータポインタへ移動
    Worksheets(SheetNo).Select
    Line = Line + 1
    LineNo = LineNo + 1
Loop
End Sub
