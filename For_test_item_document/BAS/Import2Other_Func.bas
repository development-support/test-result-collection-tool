Attribute VB_Name = "Import2Other_Func"
Option Explicit

Public Const EXPORT_MACRO_NAME As String = "Collect_Func"

'*******************************************************************************
' 「他項目書への集計機能インポート」ボタンをクリック
'
' [引数]
' なし
'
' [戻り値]
' なし
'
' [処理概要]
' (Step 1)ファイル選択ダイアログ[*.xlsm]を表示し、ユーザーに処理対象ファイルを
'         選択してもらう。
'         ※キャンセルされた場合、何もしないで終了。
' (Step 2)本試験項目書の集計マクロ「Collect_Func」を一旦エクスポートする。
' (Step 3)指定された全試験項目書において、以下(Step 4〜5)を実施する。
' (Step 4)インポート先の試験項目書に標準モジュール「Module2」,「Collect_Func」
'         が存在する場合、削除する。
' (Step 5)(Step 2)でエクスポートした「Collect_Func」をインポート先の試験項目書に
'         インポートする。
' (Step 6)エクスポートした「Collect_Func.bas」を削除する。
'*******************************************************************************
Public Sub btnModImport2Other()
  Dim StartTime As Double
  Dim EndTime As Double
  Dim TotalTime As Double
  Dim FilePath As String
  Dim FileName() As String
  Dim FileNum As Long, i As Long
  Dim MacroPath As String: MacroPath = ThisWorkbook.Path
  Dim MacroName As String: MacroName = EXPORT_MACRO_NAME & ".bas"

  ' 開始時間取得
  StartTime = Timer

  Application.ScreenUpdating = False

  ' (Step 1)ファイル選択ダイアログ[*.xlsm]を表示し、ユーザーに処理対象ファイルを
  '         選択してもらう。
  '         ※キャンセルされた場合、何もしないで終了。
  If (OpenMultiFiles(FilePath, FileName()) = False) Then
    Exit Sub
  End If

  FileNum = UBound(FileName)
  Debug.Print "(btnModImport2Other):FileNum = " & FileNum

  ' (Step 2)本試験項目書の集計マクロ「Collect_Func」を一旦エクスポートする。
  With ThisWorkbook.VBProject
    .VBComponents(EXPORT_MACRO_NAME).Export ThisWorkbook.Path & "\" & MacroName
  End With

  ' (Step 3)指定された全試験項目書において、以下(Step 4〜5)を実施する。
  For i = 1 To FileNum
    Call ImportMacro(FilePath, FileName(i), MacroPath, EXPORT_MACRO_NAME)
  Next i

  ' (Step 6)エクスポートした「Collect_Func.bas」を削除する。
  Kill MacroPath & "\" & MacroName

  Application.ScreenUpdating = True

  ' 終了時間取得
  EndTime = Timer
  TotalTime = EndTime - StartTime

  MsgBox "インポート完了" & vbCrLf & _
         "elapsed time : " & TotalTime, vbInformation

End Sub

'*******************************************************************************
' 複数ファイルの選択ダイアログを表示し、フルパス＆ファイル名を返す
'
' [引数]
'  strFilePath As String  ：選択ファイルのあるフルパスが格納される
'  strFileName() As String：選択ファイル名が格納される
' [戻り値]
'  Boolean：True  (ファイル選択された)
'         ：False (キャンセルされた)
'*******************************************************************************
Public Function OpenMultiFiles(ByRef strFilePath As String, _
                               ByRef strFileName() As String) As Boolean
  Dim openFilePath As Variant
  Dim i As Long
  Dim vTarget As Variant


  ' ダイアログボックスで選択したファイルを配列に入れる
  ' MultiSelect:=Trueにすると複数ファイル選択OK
  openFilePath = Application.GetOpenFilename("Excelマクロ(*.xlsm),*.xlsm", _
                                             , _
                                             "インポート先の試験項目書を選んで下さい", _
                                             MultiSelect:=True)
  ' ファイルが１つ以上選択されていた場合、配列が返される。
  If IsArray(openFilePath) Then
    Debug.Print "(OpenMultiFiles):UBound(openFilePath) = " & UBound(openFilePath)
    ReDim strFileName(UBound(openFilePath))

    '配列分、繰り返しファイルを取得する。
    i = 1
    For Each vTarget In openFilePath
      strFileName(i) = Dir(vTarget)
      strFilePath = Replace(vTarget, strFileName(i), "")
      Debug.Print "(OpenMultiFiles):strFileName(" & i & ") = " & strFileName(i)
      i = i + 1
    Next vTarget

    Debug.Print "(OpenMultiFiles):strFilePath = " & strFilePath
  Else
    ' キャンセルされた場合、Falseを返す。
    OpenMultiFiles = False
    Exit Function
  End If

  OpenMultiFiles = True

End Function

'*******************************************************************************
' 複数ファイルの選択ダイアログを表示し、フルパス＆ファイル名を返す
'
' [引数]
'  strFilePath As String ：対象ファイルのフルパス
'  strFileName As String ：対象ファイル名
'  strModPath As String  ：対象モジュールのフルパス
'  strModName As String  ：対象モジュール名
' [戻り値]
'  なし
'*******************************************************************************
Public Function ImportMacro(ByVal strFilePath As String, _
                            ByVal strFileName As String, _
                            ByVal strModPath As String, _
                            ByVal strModName As String)
  Const MOD2 As String = "Module2"
  Dim wb As Workbook
  Dim i As Long


  Debug.Print "(ImportMacro):" & vbCrLf & _
              " strFilePath = " & strFilePath & _
              " strFileName = " & strFileName & vbCrLf & _
              " strModPath = " & strModPath & _
              " strModName = " & strModName
  
  Set wb = Workbooks.Open(strFilePath & strFileName)

  ' (Step 4)インポート先の試験項目書に標準モジュール「Module2」,「Collect_Func」
  '         が存在する場合、削除する。
  With wb.VBProject
    For i = 1 To .VBComponents.Count
      Select Case (.VBComponents(i).Name)
      Case strModName
        ' ソースを削除
        'With .VBComponents(i).CodeModule
        '  If (.CountOfLines <> 0) Then
        '    .DeleteLines 1, .CountOfLines
        '  End If
        'End With
        ' 標準モジュールを削除
        .VBComponents.Remove .VBComponents.Item(strModName)
        Debug.Print "(ImportMacro):Delete module " & strModName
      Case MOD2
        ' ソースを削除
        'With .VBComponents(i).CodeModule
        '  If (.CountOfLines <> 0) Then
        '    .DeleteLines 1, .CountOfLines
        '  End If
        'End With
        ' 標準モジュールを削除
        .VBComponents.Remove .VBComponents.Item(MOD2)
        Debug.Print "(ImportMacro):Delete module " & MOD2
      End Select
    Next i
  End With
  ' (Step 5)(Step 2)でエクスポートした「Collect_Func」をインポート先の試験項目書に
  '         インポートする。
  With wb.VBProject.VBComponents
    .Import strModPath & "\" & strModName & ".bas"
  End With

  wb.Close savechanges:=True

End Function
