Attribute VB_Name = "Collect_Func"
Option Explicit

' 下記の2行の内のどちらかをコメントにして下さい！
#Const cnsTest = 0  ' ←本番
'#Const cnsTest = 1   ' ←テスト

' 集計対象シート名
Public Const CountingTargetSheet As String = "試験項目一覧兼結果一覧"
' 集計シート名
Public Const CountingResultSheet As String = "集計"
' 集計対象シートの見出し名
Public Const TestPriority As String = "優先度"
Public Const PlannedStartDate As String = "開始予定日"
Public Const PlannedCompDate As String = "完了予定日"
Public Const TestStartDate As String = "試験開始日"
Public Const TestCompDate As String = "試験完了日"
Public Const TestResult As String = "試験結果"
Public Const ReTestDate As String = "リトライ実施日"
Public Const ReTestResult As String = "リトライ結果"
Public Const ProblemsDetected As String = "検出問題" & vbLf & _
                                          "CT問処 No." & vbLf & _
                                          "問題処理票番号"
' 集計シートの項目行
Enum eRowItems
  Date = 1
  TotalTestsPlanned
  PlannedTests
  PlannedPass
  ActualAttempts
  Passed
  Blocked
  Failed
  FailedReady4ReTest
  ReadyNotAttempted
  Executed
  PTRsUnResolved
  PTRsResolved
  PTRsSubmittedDaily
  DailyBug
End Enum

' 集計用情報の定義型
Type tTestItemInfo
  numOfPriority As Long         ' 指定優先度の個数
  Priority() As String          ' 指定優先度(個数分)
  rowLast As Long               ' 最終行
  minDate As Date               ' 最小日付
  maxDate As Date               ' 最大日付
  rngTestPriority As Range      ' 見出し位置：優先度
  rngPlannedStartDate As Range  ' 見出し位置：開始予定日
  rngPlannedCompDate As Range   ' 見出し位置：完了予定日
  rngTestStartDate As Range     ' 見出し位置：試験開始日
  rngTestCompDate As Range      ' 見出し位置：試験完了日
  rngTestResult As Range        ' 見出し位置：試験結果
  rngReTestDate As Range        ' 見出し位置：リトライ実施日
  rngReTestResult As Range      ' 見出し位置：リトライ結果
  rngProblemsDetected As Range  ' 見出し位置：検出問題
End Type

' 集計用情報
Public gTestItemInfo As tTestItemInfo
' MsgBox表示フラグ
Public gMsgflag As Boolean

'*******************************************************************************
' 集計実行ボタンをクリック
'
' [引数]
' なし
'
' [戻り値]
' なし
'
' [処理概要]
' (Step 1)集計対象のテスト優先度の指定値チェック
'         (指定が無い場合、強制終了)
' (Step 2)集計対象シートの存在チェック
'         (集計対象シートが存在しない場合、強制終了)
' (Step 3)集計対象シートのフォーマットチェック
'         (見出し名が無い場合、強制終了)
' (Step 4)集計対象シートの最終行を取得
'         (最終行 = 見出し行の場合、強制終了[１つも項目が入力されていないとみなす])
' (Step 5)集計対象のテスト優先度個数に応じて、集計シートのExcel関数を設定する
'*******************************************************************************
Public Sub btnExeCollection(Optional Msgflag As Boolean = True)
  Dim StartTime As Double
  Dim EndTime As Double
  Dim TotalTime As Double

  ' 開始時間取得
  StartTime = Timer

  Application.ScreenUpdating = False
  Application.Calculation = xlCalculationManual

  ' Optional引数値を保存
  gMsgflag = Msgflag

  ' (Step 1)集計対象のテスト優先度の指定値チェック
  '         (指定が無い場合、強制終了)
  If (isPrioritySet() = False) Then
    Exit Sub
  End If

  ' (Step 2)集計対象シートの存在チェック
  '         (集計対象シートが存在しない場合、強制終了)
  If (isSheetExist(CountingTargetSheet) = False) Then
    MsgBox "[強制終了]集計対象シート(" & CountingTargetSheet & ")がありません", _
           vbCritical
    Exit Sub
  End If

  ' (Step 3)集計対象シートのフォーマットチェック
  '         (見出し名が無い場合、強制終了)
  If (CheckTestItemFormat(CountingTargetSheet) = False) Then
    Exit Sub
  End If

  ' (Step 4)集計対象シートの最終行を取得
  '         (最終行 = 見出し行の場合、強制終了[１つも項目が入力されていないとみなす])
  If (CheckTestItemLastRow(CountingTargetSheet) = False) Then
    Exit Sub
  End If

  ' (Step 5)集計対象のテスト優先度に応じて、集計シートのExcel関数を設定する
  ' 集計シートの値をクリア
  Call ClearCollectFunction
  ' 最大最小日付を求める
  Call CalcMaxMinDate(CountingTargetSheet)
  ' 集計シートにExcel関数を設定
  Call SetCollectFunction(CountingTargetSheet)

  Application.ScreenUpdating = True
  Application.Calculation = xlCalculationAutomatic

  ' 終了時間取得
  EndTime = Timer
  TotalTime = EndTime - StartTime

  If (gMsgflag = True) Then
    MsgBox "集計完了" & vbCrLf & _
           "elapsed time : " & TotalTime, vbInformation
  End If

End Sub

' 集計対象のテスト優先度の入力値をチェックするFunction
' [引数]
'   なし
'
' [戻り値]
'   Boolean：True  (チェックOK)
'          ：False (チェックNG)
Public Function isPrioritySet() As Boolean
  Dim strTempPri As String
  Dim vPriority As Variant
  Dim i As Long

  strTempPri = Range("TestPriority").Value
#If (cnsTest = 1) Then
  Debug.Print "(isPrioritySet):strTempPri = " & strTempPri
#End If

  If (strTempPri = "") Then
    If (gMsgflag = True) Then
      MsgBox "[強制終了]集計対象のテスト優先度が指定されていません", _
             vbCritical
    End If
    isPrioritySet = False
    Exit Function
  End If

  vPriority = Split(strTempPri, ",")
  ' カンマ区切りで無い場合
  If (LBound(vPriority) = UBound(vPriority)) Then
    ' 指定優先度の個数を保存
    gTestItemInfo.numOfPriority = 1
    ' 指定優先度を保存
    ReDim gTestItemInfo.Priority(1)
    gTestItemInfo.Priority(1) = vPriority(0)
  ' カンマ区切りの場合
  Else
    ' 指定優先度の個数を保存
    gTestItemInfo.numOfPriority = UBound(vPriority) + 1
    ' 指定優先度を保存
    ReDim gTestItemInfo.Priority(UBound(vPriority) + 1)

    For i = 0 To UBound(vPriority)
#If (cnsTest = 1) Then
      Debug.Print "(isPrioritySet):vPriority(" & i & ") = " & vPriority(i)
#End If
      gTestItemInfo.Priority(i + 1) = vPriority(i)
    Next i
  End If

#If (cnsTest = 1) Then
  Debug.Print "(isPrioritySet):numOfPriority = " & gTestItemInfo.numOfPriority
  For i = 1 To gTestItemInfo.numOfPriority
    Debug.Print "(isPrioritySet):Priority(" & i & ") = " & gTestItemInfo.Priority(i)
  Next i
#End If

  isPrioritySet = True

End Function

' 指定シートが存在するかチェックするFunction
' [引数]
'   strSheetName As String：シート名
'
' [戻り値]
'   Boolean：True  (指定シートが存在する)
'          ：False (指定シートが存在しない)
Public Function isSheetExist(ByVal strSheetName As String) As Boolean
  Dim ws As Worksheet
  Dim FindFlg As Boolean

  FindFlg = False

  For Each ws In Worksheets
    If ws.Name = strSheetName Then
      FindFlg = True
      Exit For
    End If
  Next ws

  If FindFlg = False Then
    isSheetExist = False
    Exit Function
  End If

  isSheetExist = True

End Function

' 指定行に指定見出し(文字列)が存在するかチェックするFunction
' [引数]
'   rowNum As Long：見出し検索行
'   strHeading As String：見出し名
'   rngResult As Range：検索して見つかった指定見出し位置
'
' [戻り値]
'   Boolean：True  (指定見出しが存在する)
'          ：False (指定見出しが存在しない)
Public Function CheckHeading(ByVal rowNum As Long, _
                             ByVal strHeading As String, _
                             ByRef rngResult As Range) As Boolean
  Dim FoundCell As Range

#If (cnsTest = 1) Then
  Debug.Print "(CheckHeading):rowNum = " & rowNum & " strHeading = " & strHeading
#End If

  Set FoundCell = Rows(rowNum).Find(What:=strHeading, LookAt:=xlWhole)
  If FoundCell Is Nothing Then
    CheckHeading = False
    Exit Function
  End If

  Set rngResult = FoundCell
  CheckHeading = True

End Function

' 集計対象シートのフォーマットをチェックするFunction
' [引数]
'   strSheetName As String：シート名
'
' [戻り値]
'   Boolean：True  (チェックOK)
'          ：False (チェックNG)
Public Function CheckTestItemFormat(ByVal strSheetName As String) As Boolean
  Const rowTarget As Long = 2
  Dim wsOrg As Worksheet
  Dim wsTgt As Worksheet
  Dim strNgHeading As String

#If (cnsTest = 1) Then
  Debug.Print "(CheckTestItemFormat):strSheetName = " & strSheetName
#End If

  Set wsOrg = ActiveSheet
  Set wsTgt = Worksheets(strSheetName)

  ' 指定シートに切替
  wsTgt.Activate
  ' 見出し名：優先度
  If (CheckHeading(rowTarget, _
                   TestPriority, _
                   gTestItemInfo.rngTestPriority) = False) Then
    strNgHeading = TestPriority
    GoTo CHECK_TEST_ITEM_FORMAT_NG
  End If

  ' 見出し名：開始予定日
  If (CheckHeading(rowTarget, _
                   PlannedStartDate, _
                   gTestItemInfo.rngPlannedStartDate) = False) Then
    strNgHeading = PlannedStartDate
    GoTo CHECK_TEST_ITEM_FORMAT_NG
  End If

  ' 見出し名：完了予定日
  If (CheckHeading(rowTarget, _
                   PlannedCompDate, _
                   gTestItemInfo.rngPlannedCompDate) = False) Then
    strNgHeading = PlannedCompDate
    GoTo CHECK_TEST_ITEM_FORMAT_NG
  End If

  ' 見出し名：試験開始日
  If (CheckHeading(rowTarget, _
                   TestStartDate, _
                   gTestItemInfo.rngTestStartDate) = False) Then
    strNgHeading = TestStartDate
    GoTo CHECK_TEST_ITEM_FORMAT_NG
  End If

  ' 見出し名：試験完了日
  If (CheckHeading(rowTarget, _
                   TestCompDate, _
                   gTestItemInfo.rngTestCompDate) = False) Then
    strNgHeading = TestCompDate
    GoTo CHECK_TEST_ITEM_FORMAT_NG
  End If

  ' 見出し名：試験結果
  If (CheckHeading(rowTarget, _
                   TestResult, _
                   gTestItemInfo.rngTestResult) = False) Then
    strNgHeading = TestResult
    GoTo CHECK_TEST_ITEM_FORMAT_NG
  End If

  ' 見出し名：リトライ実施日
  If (CheckHeading(rowTarget, _
                   ReTestDate, _
                   gTestItemInfo.rngReTestDate) = False) Then
    strNgHeading = ReTestDate
    GoTo CHECK_TEST_ITEM_FORMAT_NG
  End If

  ' 見出し名：リトライ結果
  If (CheckHeading(rowTarget, _
                   ReTestResult, _
                   gTestItemInfo.rngReTestResult) = False) Then
    strNgHeading = ReTestResult
    GoTo CHECK_TEST_ITEM_FORMAT_NG
  End If

  ' 見出し名：検出問題
  If (CheckHeading(rowTarget, _
                   ProblemsDetected, _
                   gTestItemInfo.rngProblemsDetected) = False) Then
    strNgHeading = ProblemsDetected
    GoTo CHECK_TEST_ITEM_FORMAT_NG
  End If

  ' OKルート
  wsOrg.Activate
  CheckTestItemFormat = True
  Exit Function

CHECK_TEST_ITEM_FORMAT_NG:
  ' NGルート
  wsOrg.Activate

  If (gMsgflag = True) Then
    MsgBox "[強制終了]" & strSheetName & _
           "シートに見出し名(" & strNgHeading & ")がありません", _
           vbCritical
  End If

  CheckTestItemFormat = False

End Function

' 集計対象シートの最終行を取得するFunction
' [引数]
'   strSheetName As String：集計対象シート名
'
' [戻り値]
'   Boolean：True  (取得成功)
'          ：False (最終行 = 見出し行になっている)
Public Function CheckTestItemLastRow(ByVal strSheetName As String) As Boolean
  Const colBase As Long = 7
  Const rowTarget As Long = 2
  Dim wsTgt As Worksheet
  Dim rowMax As Long

#If (cnsTest = 1) Then
  Debug.Print "(CheckTestItemLastRow):strSheetName = " & strSheetName
#End If

  Set wsTgt = Worksheets(strSheetName)

  ' 小項目名の記載列を基準として、最終行を求める。
  rowMax = wsTgt.Cells(Rows.Count, colBase).End(xlUp).Row

  gTestItemInfo.rowLast = rowMax
#If (cnsTest = 1) Then
  Debug.Print "(CheckTestItemLastRow):rowLast = " & gTestItemInfo.rowLast
#End If

  ' 最終行と見出し行が同じ場合、試験項目が入力されていないとみなす
  If (rowMax = rowTarget) Then
    If (gMsgflag = True) Then
      MsgBox "[強制終了]試験項目が記載されていません(rowMax:" & rowMax & ")", _
             vbCritical
    End If

    CheckTestItemLastRow = False
    Exit Function
  End If

  CheckTestItemLastRow = True

End Function

' 集計シートのデータをクリアするFunction
' [引数]
'   strSheetName As String：集計シート名
'
' [戻り値]
'   なし
Public Function ClearCollectFunction()
  Const rowBegin As Long = eRowItems.Date
  Const rowEnd As Long = eRowItems.DailyBug
  Const colBegin As Long = 2
  Dim colEnd As Long
  Dim i As Long
  Dim j As Long


  With Worksheets(CountingResultSheet)
    ' 1行目の最終列を取得
    colEnd = .Cells(eRowItems.Date, Columns.Count).End(xlToLeft).Column
#If (cnsTest = 1) Then
    Debug.Print "(ClearCollectFunction):rowBegin = " & rowBegin & _
                " rowEnd = " & rowEnd & " colBegin = " & colBegin & _
                " colEnd = " & colEnd
#End If
    ' 行見出しのみで、データが１つも無い場合、何もしない
    If (colEnd = 1) Then
      ' NOP
    Else
      .Range(.Cells(rowBegin, colBegin), .Cells(rowEnd, colEnd)).Clear
    End If
  End With

End Function

' 最大/最小日付を取得するFunction
' [引数]
'   strSheetName As String：集計対象シート名
'
' [戻り値]
'   なし
Public Function CalcMaxMinDate(ByVal strSheetName As String)
  Const rowBegin As Long = 3
  Dim wsOrg As Worksheet
  Dim wsTgt As Worksheet
  Dim tempDate(0 To 2) As Date
  Dim i As Long
  Dim tempMax As Date
  Dim minDate As Date
  Dim maxDate As Date
  Dim colPlannedStart As Long
  Dim colPlannedComp As Long
  Dim colTestStart As Long
  Dim colTestComp As Long
  Dim colReTestDate As Long
  Dim minPlannedStart As Date
  Dim maxPlannedComp As Date
  Dim minTestStart As Date
  Dim maxTestComp As Date
  Dim maxReTestDate As Date

#If (cnsTest = 1) Then
  Debug.Print "(CalcMaxMinDate):strSheetName = " & strSheetName
#End If

  Set wsOrg = ActiveSheet
  Set wsTgt = Worksheets(strSheetName)

  ' 指定シートに切替
  wsTgt.Activate
  ' 開始予定日の列を取得
  colPlannedStart = gTestItemInfo.rngPlannedStartDate.Column
  ' 完了予定日の列を取得
  colPlannedComp = gTestItemInfo.rngPlannedCompDate.Column
  ' 試験開始日の列を取得
  colTestStart = gTestItemInfo.rngTestStartDate.Column
  ' 試験完了日の列を取得
  colTestComp = gTestItemInfo.rngTestCompDate.Column
  ' リトライ実施日の列を取得
  colReTestDate = gTestItemInfo.rngReTestDate.Column

#If (cnsTest = 1) Then
  Debug.Print "(CalcMaxMinDate):colPlannedStart = " & colPlannedStart & _
              " colPlannedComp = " & colPlannedComp & _
              " colTestStart = " & colTestStart & _
              " colTestComp = " & colTestComp & _
              " colReTestDate = " & colReTestDate
#End If

  With Application.WorksheetFunction
    ' 開始予定日の最小日付を取得
    minPlannedStart = .min(Range(Cells(rowBegin, colPlannedStart), _
                                 Cells(gTestItemInfo.rowLast, colPlannedStart)))
    ' 完了予定日の最大日付を取得
    maxPlannedComp = .Max(Range(Cells(rowBegin, colPlannedComp), _
                                Cells(gTestItemInfo.rowLast, colPlannedComp)))
    ' 試験開始日の最小日付を取得
    minTestStart = .min(Range(Cells(rowBegin, colTestStart), _
                              Cells(gTestItemInfo.rowLast, colTestStart)))
    ' 試験完了日の最大日付を取得
    maxTestComp = .Max(Range(Cells(rowBegin, colTestComp), _
                             Cells(gTestItemInfo.rowLast, colTestComp)))
    ' リトライ実施日の最大日付を取得
    maxReTestDate = .Max(Range(Cells(rowBegin, colReTestDate), _
                               Cells(gTestItemInfo.rowLast, colReTestDate)))
  End With

#If (cnsTest = 1) Then
  Debug.Print "(CalcMaxMinDate):minPlannedStart = " & minPlannedStart & _
              " maxPlannedComp = " & maxPlannedComp & _
              " minTestStart = " & minTestStart & _
              " maxTestComp = " & maxTestComp & _
              " maxReTestDate = " & maxReTestDate
#End If

  With Application.WorksheetFunction
    ' 試験開始日が全て未入力の場合
    If (.Count(Range(Cells(rowBegin, colTestStart), _
                     Cells(gTestItemInfo.rowLast, colTestStart))) = 0) Then
      ' 開始予定日より最小日付を求める
      minDate = minPlannedStart
      ' 完了予定日より最大日付を求める
      maxDate = maxPlannedComp
    Else
    ' 試験開始日が入力されていた場合
      ' 開始予定日、試験開始日より最小日付を求める
      If (minPlannedStart <= minTestStart) Then
        minDate = minPlannedStart
      Else
        minDate = minTestStart
      End If
      ' リトライ実施日が入力されていない場合、完了予定日、試験完了日より最大日付を求める
      If (maxReTestDate = 0) Then
        If (maxPlannedComp <= maxTestComp) Then
          maxDate = maxTestComp
        Else
          maxDate = maxPlannedComp
        End If
      ' リトライ実施日が入力されている場合、完了予定日、試験完了日、リトライ実施日より
      ' 最大日付を求める
      Else
        tempDate(0) = maxPlannedComp
        tempDate(1) = maxTestComp
        tempDate(2) = maxReTestDate

        For i = LBound(tempDate) To UBound(tempDate)
          If (tempDate(i) > tempMax) Then
            tempMax = tempDate(i)
          End If
        Next i

        maxDate = tempMax
      End If
    End If
  End With

#If (cnsTest = 1) Then
  Debug.Print "(CalcMaxMinDate):minDate = " & minDate & " maxDate = " & maxDate
#End If
  gTestItemInfo.minDate = minDate
  gTestItemInfo.maxDate = maxDate

  wsOrg.Activate

End Function

' 集計対象のテスト優先度に応じて、集計シートのExcel関数を設定するFunction
' [引数]
'   strSheetName As String：集計対象シート名
'
' [戻り値]
'   なし
Public Function SetCollectFunction(ByVal strSheetName As String)
  Const rowBegin As Long = eRowItems.Date
  Const rowEnd As Long = eRowItems.DailyBug
  Const colBegin As Long = 2
  Dim colEnd As Long
  Dim numOfDate As Long
  Dim wsTgt As Worksheet: Set wsTgt = Worksheets(strSheetName)
  Dim i As Long
  Dim j As Long
  Dim k As Long
  Dim addrTestPriority As String
  Dim addrPlannedStart As String
  Dim addrPlannedComp As String
  Dim addrTestStart As String
  Dim addrTestComp As String
  Dim addrTestResult As String
  Dim addrReTestDate As String
  Dim addrReTestResult As String
  Dim addrProblemsDetected As String
  Dim strTemp As String
  Dim CellArray() As Variant
  Dim colIndex As Long

#If (cnsTest = 1) Then
  Debug.Print "(SetCollectFunction):strSheetName = " & strSheetName
#End If

  ' 最大/最小日付より、試験期間の日数を算出
  numOfDate = (gTestItemInfo.maxDate - gTestItemInfo.minDate) + 1
  colEnd = numOfDate + 1
#If (cnsTest = 1) Then
  Debug.Print "(SetCollectFunction):numOfDate = " & numOfDate & " colEnd = " & colEnd
#End If

  With wsTgt
    ' 優先度の検索範囲を算出
    addrTestPriority = .Cells(gTestItemInfo.rngTestPriority.Row + 1, _
                              gTestItemInfo.rngTestPriority.Column).Address & ":" & _
                       .Cells(gTestItemInfo.rowLast, _
                              gTestItemInfo.rngTestPriority.Column).Address
    ' 開始予定日の検索範囲を算出
    addrPlannedStart = .Cells(gTestItemInfo.rngPlannedStartDate.Row + 1, _
                              gTestItemInfo.rngPlannedStartDate.Column).Address & ":" & _
                       .Cells(gTestItemInfo.rowLast, _
                              gTestItemInfo.rngPlannedStartDate.Column).Address
    ' 完了予定日の検索範囲を算出
    addrPlannedComp = .Cells(gTestItemInfo.rngPlannedCompDate.Row + 1, _
                             gTestItemInfo.rngPlannedCompDate.Column).Address & ":" & _
                      .Cells(gTestItemInfo.rowLast, _
                             gTestItemInfo.rngPlannedCompDate.Column).Address
    ' 試験開始日の検索範囲を算出
    addrTestStart = .Cells(gTestItemInfo.rngTestStartDate.Row + 1, _
                           gTestItemInfo.rngTestStartDate.Column).Address & ":" & _
                    .Cells(gTestItemInfo.rowLast, _
                           gTestItemInfo.rngTestStartDate.Column).Address
    ' 試験完了日の検索範囲を算出
    addrTestComp = .Cells(gTestItemInfo.rngTestCompDate.Row + 1, _
                          gTestItemInfo.rngTestCompDate.Column).Address & ":" & _
                   .Cells(gTestItemInfo.rowLast, _
                          gTestItemInfo.rngTestCompDate.Column).Address
    ' 試験結果の検索範囲を算出
    addrTestResult = .Cells(gTestItemInfo.rngTestResult.Row + 1, _
                            gTestItemInfo.rngTestResult.Column).Address & ":" & _
                     .Cells(gTestItemInfo.rowLast, _
                            gTestItemInfo.rngTestResult.Column).Address
    ' リトライ実施日の検索範囲を算出
    addrReTestDate = .Cells(gTestItemInfo.rngReTestDate.Row + 1, _
                            gTestItemInfo.rngReTestDate.Column).Address & ":" & _
                     .Cells(gTestItemInfo.rowLast, _
                            gTestItemInfo.rngReTestDate.Column).Address
    ' リトライ結果の検索範囲を算出
    addrReTestResult = .Cells(gTestItemInfo.rngReTestResult.Row + 1, _
                              gTestItemInfo.rngReTestResult.Column).Address & ":" & _
                       .Cells(gTestItemInfo.rowLast, _
                              gTestItemInfo.rngReTestResult.Column).Address
    ' 検出問題の検索範囲を算出
    addrProblemsDetected = .Cells(gTestItemInfo.rngProblemsDetected.Row + 1, _
                                  gTestItemInfo.rngProblemsDetected.Column).Address & ":" & _
                           .Cells(gTestItemInfo.rowLast, _
                                  gTestItemInfo.rngProblemsDetected.Column).Address
  End With
#If (cnsTest = 1) Then
  Debug.Print "(SetCollectFunction):addrTestPriority = " & addrTestPriority & _
              " addrPlannedStart = " & addrPlannedStart & _
              " addrPlannedComp = " & addrPlannedComp & vbCrLf & _
              "                     addrTestStart = " & addrTestStart & _
              " addrTestComp = " & addrTestComp & _
              " addrTestResult = " & addrTestResult & vbCrLf & _
              "                     addrReTestDate = " & addrReTestDate & _
              " addrReTestResult = " & addrReTestResult & _
              " addrProblemsDetected = " & addrProblemsDetected
#End If

  ' セル一括代入用配列を定義
  ReDim CellArray(rowBegin To rowEnd, 1 To colEnd - colBegin + 1)

  With Worksheets(CountingResultSheet)
  ' 行
  For i = rowBegin To rowEnd
    ' セル一括代入用配列の列インデックスを初期化
    colIndex = 1

    ' 列
    For j = colBegin To colEnd
      Select Case (i)
      ' Date
      Case eRowItems.Date
        ' 最初の列の場合
        If (j = colBegin) Then
          strTemp = "=IF(COUNT(" & CountingTargetSheet & "!" & addrTestStart & ")=0," & _
                    "MIN(" & CountingTargetSheet & "!" & addrPlannedStart & ")," & _
                    "IF(MIN(" & CountingTargetSheet & "!" & addrPlannedStart & ")<" & _
                    "MIN(" & CountingTargetSheet & "!" & addrTestStart & ")," & _
                    "MIN(" & CountingTargetSheet & "!" & addrPlannedStart & ")," & _
                    "MIN(" & CountingTargetSheet & "!" & addrTestStart & ")))"
        Else
          strTemp = "=" & .Cells(i, j - 1).Address(False, False) & "+1"
        End If

      ' Total Tests Planned
      Case eRowItems.TotalTestsPlanned
        ' 最初の列の場合
        If (j = colBegin) Then
          ' 指定したテスト優先度数分、カウント
          For k = LBound(gTestItemInfo.Priority) To UBound(gTestItemInfo.Priority)
            If (k = 1) Then
              strTemp = "=COUNTIF(" & CountingTargetSheet & "!" & addrTestPriority & "," & _
                        """" & gTestItemInfo.Priority(k) & """)"
            Else
              strTemp = strTemp & "+" & _
                        "COUNTIF(" & CountingTargetSheet & "!" & addrTestPriority & "," & _
                        """" & gTestItemInfo.Priority(k) & """)"
            End If
          Next k
        Else
          strTemp = "=" & .Cells(i, colBegin).Address(False, False)
        End If

      ' Planned Tests
      Case eRowItems.PlannedTests
        ' 指定したテスト優先度数分、カウント
        For k = LBound(gTestItemInfo.Priority) To UBound(gTestItemInfo.Priority)
          If (k = 1) Then
            strTemp = "=COUNTIFS(" & CountingTargetSheet & "!" & addrPlannedStart & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """)"
          Else
            strTemp = strTemp & "+" & _
                      "COUNTIFS(" & CountingTargetSheet & "!" & addrPlannedStart & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """)"
          End If
        Next k
        ' 最初の列以外の場合
        If (j > colBegin) Then
          strTemp = strTemp & "+" & .Cells(i, j - 1).Address(False, False)
        End If

      ' Planned Pass
      Case eRowItems.PlannedPass
        ' 指定したテスト優先度数分、カウント
        For k = LBound(gTestItemInfo.Priority) To UBound(gTestItemInfo.Priority)
          If (k = 1) Then
            strTemp = "=COUNTIFS(" & CountingTargetSheet & "!" & addrPlannedComp & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """)"
          Else
            strTemp = strTemp & "+" & _
                      "COUNTIFS(" & CountingTargetSheet & "!" & addrPlannedComp & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """)"
          End If
        Next k
        ' 最初の列以外の場合
        If (j > colBegin) Then
          strTemp = strTemp & "+" & .Cells(i, j - 1).Address(False, False)
        End If

      ' Actual Attempts
      Case eRowItems.ActualAttempts
        ' Executedの値を参照
        strTemp = "=" & .Cells(11, j).Address(False, False)

      ' Passed
      Case eRowItems.Passed
        ' 指定したテスト優先度数分、カウント
        For k = LBound(gTestItemInfo.Priority) To UBound(gTestItemInfo.Priority)
          If (k = 1) Then
            strTemp = "=COUNTIFS(" & CountingTargetSheet & "!" & addrTestStart & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """OK"")" & _
                      "+COUNTIFS(" & CountingTargetSheet & "!" & addrReTestDate & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrReTestResult & "," & _
                      """OK"")"
          Else
            strTemp = strTemp & "+" & _
                      "COUNTIFS(" & CountingTargetSheet & "!" & addrTestStart & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """OK"")" & _
                      "+COUNTIFS(" & CountingTargetSheet & "!" & addrReTestDate & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrReTestResult & "," & _
                      """OK"")"
          End If
        Next k
        ' 最初の列以外の場合
        If (j > colBegin) Then
          strTemp = strTemp & "+" & .Cells(i, j - 1).Address(False, False)
        End If

      ' Blocked
      Case eRowItems.Blocked
        ' 指定したテスト優先度数分、カウント
        For k = LBound(gTestItemInfo.Priority) To UBound(gTestItemInfo.Priority)
          If (k = 1) Then
            strTemp = "=COUNTIFS(" & CountingTargetSheet & "!" & addrTestStart & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """BLK"")" & _
                      "-COUNTIFS(" & CountingTargetSheet & "!" & addrReTestDate & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrReTestResult & "," & _
                      """OK""" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """BLK"")"
          Else
            strTemp = strTemp & "+" & _
                      "COUNTIFS(" & CountingTargetSheet & "!" & addrTestStart & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """BLK"")" & _
                      "-COUNTIFS(" & CountingTargetSheet & "!" & addrReTestDate & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrReTestResult & "," & _
                      """OK""" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """BLK"")"
          End If
        Next k
        ' 最初の列以外の場合
        If (j > colBegin) Then
          strTemp = strTemp & "+" & .Cells(i, j - 1).Address(False, False)
        End If

      ' Failed
      Case eRowItems.Failed
        ' 指定したテスト優先度数分、カウント
        For k = LBound(gTestItemInfo.Priority) To UBound(gTestItemInfo.Priority)
          If (k = 1) Then
            strTemp = "=COUNTIFS(" & CountingTargetSheet & "!" & addrTestStart & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """NG"")" & _
                      "-COUNTIFS(" & CountingTargetSheet & "!" & addrReTestDate & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrReTestResult & "," & _
                      """OK""" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """NG"")"
          Else
            strTemp = strTemp & "+" & _
                      "COUNTIFS(" & CountingTargetSheet & "!" & addrTestStart & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """NG"")" & _
                      "-COUNTIFS(" & CountingTargetSheet & "!" & addrReTestDate & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrReTestResult & "," & _
                      """OK""" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """NG"")"
          End If
        Next k
        ' 最初の列以外の場合
        If (j > colBegin) Then
          strTemp = strTemp & "+" & .Cells(i, j - 1).Address(False, False)
        End If

      ' Failed (Ready for ReTest)
      Case eRowItems.FailedReady4ReTest
        ' 指定したテスト優先度数分、カウント
        For k = LBound(gTestItemInfo.Priority) To UBound(gTestItemInfo.Priority)
          If (k = 1) Then
            strTemp = "=COUNTIFS(" & CountingTargetSheet & "!" & addrTestStart & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """READY"")" & _
                      "-COUNTIFS(" & CountingTargetSheet & "!" & addrReTestDate & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrReTestResult & "," & _
                      """OK""" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """READY"")"
          Else
            strTemp = strTemp & "+" & _
                      "COUNTIFS(" & CountingTargetSheet & "!" & addrTestStart & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """READY"")" & _
                      "-COUNTIFS(" & CountingTargetSheet & "!" & addrReTestDate & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrReTestResult & "," & _
                      """OK""" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """READY"")"
          End If
        Next k
        ' 最初の列以外の場合
        If (j > colBegin) Then
          strTemp = strTemp & "+" & .Cells(i, j - 1).Address(False, False)
        End If

      ' Ready (Not Attempted)
      Case eRowItems.ReadyNotAttempted
        ' (Total Tests Planned) - (Actual Attempts) - (Blocked)
        strTemp = "=" & _
                  .Cells(eRowItems.TotalTestsPlanned, j).Address(False, False) & "-" & _
                  .Cells(eRowItems.ActualAttempts, j).Address(False, False) & "-" & _
                  .Cells(eRowItems.Blocked, j).Address(False, False)

      ' Executed
      Case eRowItems.Executed
        ' (Passed) + (Failed) + (Failed (Ready for ReTest))
        strTemp = "=" & _
                  .Cells(eRowItems.Passed, j).Address(False, False) & "+" & _
                  .Cells(eRowItems.Failed, j).Address(False, False) & "+" & _
                  .Cells(eRowItems.FailedReady4ReTest, j).Address(False, False)
      ' CR/MJ PTRs UnResolved
      Case eRowItems.PTRsUnResolved
        ' 指定したテスト優先度数分、カウント
        For k = LBound(gTestItemInfo.Priority) To UBound(gTestItemInfo.Priority)
          If (k = 1) Then
            strTemp = "=COUNTIFS(" & CountingTargetSheet & "!" & addrTestStart & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """NG""" & _
                      "," & _
                      CountingTargetSheet & "!" & addrReTestResult & "," & _
                      """""" & _
                      "," & _
                      CountingTargetSheet & "!" & addrProblemsDetected & "," & _
                      """*"")"
          Else
            strTemp = strTemp & "+" & _
                      "COUNTIFS(" & CountingTargetSheet & "!" & addrTestStart & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """NG""" & _
                      "," & _
                      CountingTargetSheet & "!" & addrReTestResult & "," & _
                      """""" & _
                      "," & _
                      CountingTargetSheet & "!" & addrProblemsDetected & "," & _
                      """*"")"
          End If
        Next k
        ' 最初の列以外の場合
        If (j > colBegin) Then
          strTemp = strTemp & "+" & .Cells(i, j - 1).Address(False, False)
        End If

      ' CR/MJ PTRs Resolved
      Case eRowItems.PTRsResolved
        ' 指定したテスト優先度数分、カウント
        For k = LBound(gTestItemInfo.Priority) To UBound(gTestItemInfo.Priority)
          If (k = 1) Then
            strTemp = "=COUNTIFS(" & CountingTargetSheet & "!" & addrTestStart & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """NG""" & _
                      "," & _
                      CountingTargetSheet & "!" & addrReTestResult & "," & _
                      """OK""" & _
                      "," & _
                      CountingTargetSheet & "!" & addrProblemsDetected & "," & _
                      """*"")"
          Else
            strTemp = strTemp & "+" & _
                      "COUNTIFS(" & CountingTargetSheet & "!" & addrTestStart & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """NG""" & _
                      "," & _
                      CountingTargetSheet & "!" & addrReTestResult & "," & _
                      """OK""" & _
                      "," & _
                      CountingTargetSheet & "!" & addrProblemsDetected & "," & _
                      """*"")"
          End If
        Next k
        ' 最初の列以外の場合
        If (j > colBegin) Then
          strTemp = strTemp & "+" & .Cells(i, j - 1).Address(False, False)
        End If

      ' CR/MJ PTRs Submitted Daily
      Case eRowItems.PTRsSubmittedDaily
        ' 指定したテスト優先度数分、カウント
        For k = LBound(gTestItemInfo.Priority) To UBound(gTestItemInfo.Priority)
          If (k = 1) Then
            strTemp = "=COUNTIFS(" & CountingTargetSheet & "!" & addrTestStart & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """NG""" & _
                      "," & _
                      CountingTargetSheet & "!" & addrProblemsDetected & "," & _
                      """*"")"
          Else
            strTemp = strTemp & "+" & _
                      "COUNTIFS(" & CountingTargetSheet & "!" & addrTestStart & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """NG""" & _
                      "," & _
                      CountingTargetSheet & "!" & addrProblemsDetected & "," & _
                      """*"")"
          End If
        Next k

      ' Daily Bug
      Case eRowItems.DailyBug
        ' 指定したテスト優先度数分、カウント
        For k = LBound(gTestItemInfo.Priority) To UBound(gTestItemInfo.Priority)
          If (k = 1) Then
            strTemp = "=COUNTIFS(" & CountingTargetSheet & "!" & addrTestStart & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """NG"")"
          Else
            strTemp = strTemp & "+" & _
                      "COUNTIFS(" & CountingTargetSheet & "!" & addrTestStart & "," & _
                      CountingResultSheet & "!" & .Cells(rowBegin, j).Address(False, False) & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestPriority & "," & _
                      """" & gTestItemInfo.Priority(k) & """" & _
                      "," & _
                      CountingTargetSheet & "!" & addrTestResult & "," & _
                      """NG"")"
          End If
        Next k

      End Select

      ' セル一括代入用配列に値を代入
      CellArray(i, colIndex) = strTemp
      colIndex = colIndex + 1
    Next j
  Next i

    ' セルに一括代入して、再計算
    .Range(.Cells(rowBegin, colBegin), .Cells(rowEnd, colEnd)) = CellArray
    .Calculate

    ' 罫線を引く
    .Range(.Cells(rowBegin, colBegin), .Cells(rowEnd, colEnd)).Borders.LineStyle = xlContinuous
    ' Font設定
    .Range(.Cells(rowBegin, colBegin), .Cells(rowEnd, colEnd)).Font.Name = "Meiryo UI"
    ' Dateの書式設定
    .Range(.Cells(rowBegin, colBegin), .Cells(rowBegin, colEnd)).NumberFormatLocal = "yyyy/mm/dd"
  End With

End Function

