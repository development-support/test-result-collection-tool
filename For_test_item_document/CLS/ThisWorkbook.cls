VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ThisWorkbook"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Option Explicit

Private Sub Workbook_Open()
  Dim ws As Worksheet

  ' 開いた時のシートを保存
  Set ws = ActiveSheet

  ' 集計実行
  Call btnExeCollection(False)

  ' 元のシートに切替
  ws.Activate

End Sub
